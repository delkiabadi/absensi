<?php 
class Posisi_keuangan_m extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	
	public function get_realisasi($limit=0,$offset=0,$bulan,$tahun)
	{
		$sql ="SELECT
					c.id_m_akun,
					c.nama_akun,
					a.bulan,
					a.tahun,
					a.nominal
				FROM
					t_posisi_keuangan a
				JOIN m_posisi_keuangan b ON a.id_m_akun = b.id_m_akun
				JOIN m_akun c ON b.id_m_akun = c.id_m_akun
				WHERE 
					a.bulan = {$bulan}
					AND a.tahun = {$tahun}
				ORDER BY a.id_m_akun asc		
				";
		if($limit !=0)
			$sql .= "LIMIT {$limit} OFFSET {$offset}";
		
		return $this->db->query($sql);	
	}
	public function get_rkap($limit=0,$offset=0,$tahun)
	{
		$sql ="SELECT
					c.id_m_akun,
					c.nama_akun,					
					a.tahun,
					a.nominal
				FROM
					t_posisi_keuangan_rkap a
				JOIN m_posisi_keuangan b ON a.id_m_akun = b.id_m_akun
				JOIN m_akun c ON b.id_m_akun = c.id_m_akun
				WHERE 
					a.tahun = {$tahun}
				ORDER BY a.id_m_akun asc		
				";
		if($limit !=0)
			$sql .= "LIMIT {$limit} OFFSET {$offset}";
		
		return $this->db->query($sql);	
	}
	
	public function get_akun(){
		$sql = "SELECT
					a.id_m_akun,
					a.nama_akun
				FROM 
					m_akun a 
				JOIN m_posisi_keuangan b ON a.id_m_akun = b.id_m_akun	
				";
		return $this->db->query($sql);		
	}
	
	public function get_akun2(){
		$sql = "SELECT
					a.id_m_akun,
					a.nama_akun
				FROM 
					m_akun a 
				JOIN m_laba_rugi b ON a.id_m_akun = b.id_m_akun	
				";
		return $this->db->query($sql);		
	}
	
	public function get_laba_rugi($limit=0,$offset=0,$bulan,$tahun){
		$sql ="SELECT
					c.id_m_akun,
					c.nama_akun,
					a.bulan,
					a.tahun,
					a.nominal
				FROM
					t_laba_rugi a
				JOIN m_laba_rugi b ON a.id_m_akun = b.id_m_akun
				JOIN m_akun c ON b.id_m_akun = c.id_m_akun
				WHERE 
					a.bulan = {$bulan}
					AND a.tahun = {$tahun}
				ORDER BY a.id_m_akun asc		
				";
		if($limit !=0)
			$sql .= "LIMIT {$limit} OFFSET {$offset}";		
		return $this->db->query($sql);	
	}
	
	public function laba_rugi_rkap($limit=0,$offset=0,$tahun)
	{
		$sql ="SELECT
					c.id_m_akun,
					c.nama_akun,					
					a.tahun,
					a.nominal
				FROM
					t_laba_rugi_rkap a
				JOIN m_laba_rugi b ON a.id_m_akun = b.id_m_akun
				JOIN m_akun c ON b.id_m_akun = c.id_m_akun
				WHERE 
					a.tahun = {$tahun}
				ORDER BY a.id_m_akun asc		
				";
		if($limit !=0)
			$sql .= "LIMIT {$limit} OFFSET {$offset}";
		
		return $this->db->query($sql);	
	}
}