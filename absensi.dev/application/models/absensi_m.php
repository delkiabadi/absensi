<?php 
class Absensi_m extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function list_absensi($limit=0,$offset=0,$date_start,$date_end,$name='')
	{
		$filter = array();	
		$sql = "SELECT 
					a.id_m_pegawai,
					count((case when jam_datang > '07:30:00;' then 1 END)) as telat,
					count((case when (jam_datang is null AND jam_pulang is not null) then 1 END)) as tdk_absen_datang,
					count((case when (jam_datang is not null AND jam_pulang is null) then 1 END)) as tdk_absen_pulang,
					count((case when (jam_datang is null AND jam_pulang is null) then 1 END)) as tdk_hadir,
					count((case when (lower(to_char(tanggal,'Dy')) !='sun' AND lower(to_char(tanggal,'Dy')) !='sat' )  then 1 END)) as hari_kerja,
					max(b.nama_pegawai) as nama,					
					count(id_t_absensi) as total_absen	
				FROM	
					t_absensi a,
					m_pegawai b					
				WHERE
					a.id_m_pegawai = b.id_m_pegawai
					AND tanggal BETWEEN '{$date_start}' AND '{$date_end}'
					NAME					
				GROUP BY 
					a.id_m_pegawai
				order by max(b.nama_pegawai)
				LIMIT_OFFSET	
				";
		if($name !='')
		{
			$sql = str_replace('NAME'," AND b.nama_pegawai like '%?%'",$sql);
			array_push($filter,$name);
			
		}else
			$sql = str_replace('NAME'," ",$sql);
		
		if($limit !=0)
		{
			$sql = str_replace('LIMIT_OFFSET',' LIMIT ? OFFSET ? ',$sql);
			array_push($filter,$limit);
			array_push($filter,$offset);
		}else
			$sql = str_replace('LIMIT_OFFSET',' ',$sql);
		return $this->db->query($sql,$filter);
	}
	function absen_for_month($limit=0,$offset=0,$id,$start,$end)
	{
		$sql = "SELECT 
					a.*,
					b.id_m_pegawai,
					b.nama_pegawai
				FROM	
					t_absensi a
				JOIN	
					m_pegawai b ON a.id_m_pegawai = b.id_m_pegawai
				WHERE 
					b.id_m_pegawai = {$id}
					AND a.tanggal BETWEEN '{$start}' AND '{$end}'
					order by a.tanggal
					LIMIT_OFFSET
				";
		$filter = array();		
		if($limit !=0)
		{
			$sql = str_replace('LIMIT_OFFSET',' LIMIT ? OFFSET ? ',$sql);
			array_push($filter,$limit);
			array_push($filter,$offset);
		}else
			$sql = str_replace('LIMIT_OFFSET',' ',$sql);		
		return $this->db->query($sql,$filter);		
	}
}