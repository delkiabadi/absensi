<?php
class Akuntansi_m extends CI_Model{
	public function detail($limit=0,$offset=0,$bulan,$tahun,$kanwil='',$unit_kerja=''){
		$sql ="	SELECT
					a.id_m_cabang,
					a.nama_cabang,
					b.id_m_kanwil,
					b.nama_kanwil,
					c.nominal as investasi,
					d.nominal as klaim,
					e.nominal as subrogasi
				FROM m_cabang a 
				LEFT JOIN m_kanwil b ON a.id_m_kanwil = b.id_m_kanwil  
				LEFT JOIN t_investasi c ON a.id_m_cabang = c.id_m_cabang 
				LEFT JOIN t_klaim d ON a.id_m_cabang = d.id_m_cabang
				LEFT JOIN t_subrogasi e ON a.id_m_cabang  = e.id_m_cabang
				WHERE 
					c.bulan = ? 
					AND c.tahun = ?
				KANWIL_FLAG
				UNIT_FLAG
				ORDER BY nama_kanwil,nama_cabang
				LIMIT_OFFSET	
			";
		$filter = array();
		array_push($filter,$bulan);
		array_push($filter,$tahun);	
		if($kanwil)
		{
			$sql = str_replace('KANWIL_FLAG',' AND a.id_m_kanwil = ?',$sql);
			array_push($filter,$kanwil);
		}
		else 
			$sql = str_replace('KANWIL_FLAG',' ',$sql);
		
		if($unit_kerja)
		{
			$sql = str_replace('UNIT_FLAG',' AND a.id_m_cabang = ?',$sql);
			array_push($filter,$unit_kerja);
		}else
			$sql = str_replace('UNIT_FLAG',' ',$sql);
		
		if($limit !=0)
		{
			$sql = str_replace('LIMIT_OFFSET',' LIMIT ? OFFSET ? ',$sql);
			array_push($filter,$limit);
			array_push($filter,$offset);
		}else
			$sql = str_replace('LIMIT_OFFSET',' ',$sql);
		//print_r($sql);
		return $this->db->query($sql,$filter);		
	}
	
	public function get_rkap($limit=0,$offset=0,$tahun,$kanwil='',$unit_kerja=''){
		$sql ="	
				SELECT
					a.id_m_cabang,
					a.nama_cabang as nama_cabang,
					b.id_m_kanwil as id_m_kanwil,
					b.nama_kanwil as nama_kanwil,
					coalesce(inv.investasi,0) as investasi,
					klm.klaim as klaim,
					subro.subrogasi as subrogasi
				FROM m_cabang a 
				LEFT JOIN m_kanwil b ON a.id_m_kanwil = b.id_m_kanwil 
				LEFT JOIN (
					SELECT id_m_cabang,sum(investasi) as investasi FROM t_rkap_investasi  WHERE tahun = {$tahun} group by id_m_cabang 
				)inv ON a.id_m_cabang = inv.id_m_cabang
				LEFT JOIN (
					SELECT id_m_cabang,sum(klaim) as klaim FROM t_rkap_klaim WHERE tahun = {$tahun} group by id_m_cabang 
				) klm ON a.id_m_cabang = klm.id_m_cabang
				LEFT JOIN (
					SELECT id_m_cabang,sum(subrogasi) as subrogasi FROM t_rkap_subrogasi WHERE tahun = {$tahun} group by id_m_cabang 
				) subro ON a.id_m_cabang = subro.id_m_cabang	
				WHERE 1=1
				KANWIL_FLAG
				UNIT_FLAG
				ORDER BY nama_kanwil,nama_cabang
				LIMIT_OFFSET
			";
		$filter = array();	
		if($kanwil)
		{
			$sql = str_replace('KANWIL_FLAG',' AND a.id_m_kanwil = ?',$sql);
			array_push($filter,$kanwil);
		}
		else 
			$sql = str_replace('KANWIL_FLAG',' ',$sql);
		
		if($unit_kerja)
		{
			$sql = str_replace('UNIT_FLAG',' AND a.id_m_cabang = ?',$sql);
			array_push($filter,$unit_kerja);
		}else
			$sql = str_replace('UNIT_FLAG',' ',$sql);
		
		if($limit !=0)
		{
			$sql = str_replace('LIMIT_OFFSET',' LIMIT ? OFFSET ? ',$sql);
			array_push($filter,$limit);
			array_push($filter,$offset);
		}else
			$sql = str_replace('LIMIT_OFFSET',' ',$sql);
		//print_r($sql);
		return $this->db->query($sql,$filter);		
	}
	// get periode
	public function get_periode(){
		$sql = "SELECT * from t_periode where id_periode = 2";
		return $this->db->query($sql);
	}
	
}