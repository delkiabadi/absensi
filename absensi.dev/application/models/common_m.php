<?php 
class Common_m extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	
	public function updated($tabel,$data,$where){
		$this->db->where($where);
		return $this->db->update($tabel,$data);
			//return $this->db->affected_rows();
		//else
			//return false;
	}
	public function inserted($table,$data){
		if($this->db->insert($table,$data))
			return $this->db->affected_rows();
		else
			return false;	
	}
	
	public function inserted_id($table,$data){
		if($this->db->insert($table,$data))
		{
			return $this->db->query('select max(id_t_status) as last_id from t_status')->row()->last_id;
		}	
		else
			return false;	
	}
	
	public function inserted_batch($table,$data){
		if($this->db->insert_batch($table,$data))
			return $this->db->affected_rows();
		else
			return false;
	}
	
	public function deleted($table,$where = array()){
		$this->db->delete($table, $where); 
	}
	
	public function sql_query($query)
	{
		return $this->db->query($query);
	}
	
	public function get_where($table,$select,$where){
		return $this->db->query("select {$select} FROM {$table} where {$where} ");
	}
	
	public function trunct($table){
		if($this->db->truncate($table))
			return true;
		else
			return false;
	}
	public function get_table_where($limit=0,$offset=0,$table,$select,$where)
	{
		$sql = "SELECT {$select} FROM {$table} where {$where} ";
		if($limit !=0)
			$sql .= "LIMIT {$limit} OFFSET {$offset} ";
		return $this->db->query($sql);	
	}
	public function update_where($table,$data=array(),$where=array())
	{
		if($this->db->update($table, $data, $where))
			return true;
		else	
			return false;
	}
	
	public function get_table($table){
		return $this->db->get($table);
	}
	public function number_record($table,$where)
	{
		$sql = "select * from {$table} where 1=1 AND {$where} ";
		return  $this->db->query($sql)->num_rows();
		
	}
}
