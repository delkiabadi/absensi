<?php 
class Operasional_m extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	
	public function korporat_volume($tahun,$type=0,$kanwil='')
	{
		$product = array('',' and a.id_m_produk != 15',' and a.id_m_produk = 15');
			
		$sql = "	SELECT
						a.id_m_kanwil,
						a.nama_kanwil,
						(CASE WHEN b.percent IS NULL THEN 0 ELSE b.percent END),
						(CASE WHEN b.rkap IS NULL THEN 0 ELSE b.rkap END),
						(CASE WHEN b.pencapaian IS NULL THEN 0 ELSE b.pencapaian END)
					FROM 
						m_kanwil a 
					LEFT JOIN 
					(SELECT 
						a.id_m_kanwil, 
						(CASE 
							WHEN COALESCE(SUM(c.volume),0) = 0 THEN 0
							ELSE round((SUM(b.volume)/SUM(c.volume)),2)
						END) as percent, 
						((SUM(c.volume / 1000000)))::numeric AS rkap, 
							( (SUM(b.volume / 1000000)))::numeric AS pencapaian 
					FROM 
						m_cabang a 
					LEFT JOIN 
						( SELECT 
									a.id_m_cabang, SUM(a.volume) AS volume 
							FROM 
									t_pencapaian_vol_ijp a 
							WHERE 
									1=1 
									PRODUCT_FLAG 
							GROUP BY 
									a.id_m_cabang 
						) b ON b.id_m_cabang = a.id_m_cabang 
					LEFT JOIN 
						( 	SELECT 
									a.id_m_cabang, 
									SUM(a.volume) AS volume 
							FROM 
									t_rkap_vol_ijp a 
							JOIN 
									m_tahun b ON b.id_m_tahun = a.id_m_tahun 
							WHERE 
									b.tahun = ? 
									PRODUCT_FLAG 
							GROUP BY 
									a.id_m_cabang 
							ORDER BY 
									a.id_m_cabang 
						) c ON c.id_m_cabang = a.id_m_cabang 
					WHERE 
						(b.volume > 0 OR c.volume > 0) 
					GROUP BY 
							a.id_m_kanwil 
					ORDER BY id_m_kanwil asc 
		) b ON b.id_m_kanwil = a.id_m_kanwil";
		
		$sql = str_replace('PRODUCT_FLAG',$product[$type],$sql);
		return $this->db->query($sql,array($tahun));
	}
	
	public function korporat_ijp($tahun,$type=0,$kanwil='')
	{
		$product = array('',' and a.id_m_produk != 15',' and a.id_m_produk = 15');
		$sql = "	SELECT 
						a.id_m_kanwil, 
						a.nama_kanwil, 
						(CASE WHEN b.percent IS NULL THEN 0 ELSE b.percent END), 
						(CASE WHEN b.rkap IS NULL THEN 0 ELSE b.rkap END),
						(CASE WHEN b.pencapaian IS NULL THEN 0 ELSE b.pencapaian END)
					FROM
						m_kanwil a 
					LEFT JOIN
						( SELECT 
								a.id_m_kanwil,
								(CASE 
									WHEN COALESCE(SUM(c.volume),0) = 0 THEN 0
									ELSE round((SUM(b.volume)/SUM(c.volume)),2)
								END) as percent, 
								( (SUM(c.volume / 1000000)))::numeric AS rkap, 
								( (SUM(b.volume / 1000000)))::numeric AS pencapaian 
						FROM 
							m_cabang a 
						LEFT JOIN 
							(SELECT 
									a.id_m_cabang, 
									SUM(a.ijp_accrual) AS volume 
							FROM 
									t_pencapaian_vol_ijp a 
						WHERE 1=1 
								PRODUCT_FLAG 
						GROUP BY 
								a.id_m_cabang 
						ORDER BY 
								a.id_m_cabang 
						) b ON b.id_m_cabang = a.id_m_cabang 
					LEFT JOIN 
						( 
							SELECT 
								a.id_m_cabang, SUM(a.ijp_accrual) AS volume 
							FROM 
								t_rkap_vol_ijp a 
								JOIN m_tahun b ON b.id_m_tahun = a.id_m_tahun 
							WHERE 
								b.tahun = ? 
								PRODUCT_FLAG 
							GROUP BY 
								a.id_m_cabang 
							ORDER BY 
								a.id_m_cabang 
							) c ON c.id_m_cabang = a.id_m_cabang 
					WHERE 
						(b.volume > 0 OR c.volume > 0) 
					GROUP BY 
							a.id_m_kanwil 
					ORDER BY 
						a.id_m_kanwil 
					)b ON a.id_m_kanwil = b.id_m_kanwil ";
		
		$sql = str_replace('PRODUCT_FLAG',$product[$type],$sql);
		return $this->db->query($sql,array($tahun));	
	}
	public function korporat($limit=0,$offset=0,$type=0,$kanwil='',$unit_kerja='',$tanggal){
		$product = array('',' and a.id_m_produk != 15',' and a.id_m_produk = 15');
		$sql = "SELECT
					a.id_m_kanwil,
					a.id_m_cabang,
					max(a.nama_cabang) as nama_cabang,
					MIN(a.id_m_kanwil) as id_m_kanwil,
					SUM(b.volume) as volume,
					SUM(b.ijp) as ijp,
					max(c.nama_kanwil) as nama_kanwil	
				FROM 
					m_cabang a 
				LEFT JOIN ( 
					SELECT 
						a.id_m_cabang,
						SUM(a.ijp_accrual) AS ijp,
						SUM(a.volume) AS volume	
					FROM 
						t_pencapaian_vol_ijp a 
					WHERE 
						tanggal = '{$tanggal}'
						PRODUCT_FLAG 
					GROUP BY 
						a.id_m_cabang 
					ORDER BY 
						a.id_m_cabang 
				) b ON b.id_m_cabang = a.id_m_cabang 
				LEFT JOIN m_kanwil c on a.id_m_kanwil = c.id_m_kanwil
				WHERE 
					1=1 
					KANWIL_FLAG 
					UNIT_FLAG 
				GROUP BY 
					a.id_m_cabang 	
				ORDER BY nama_kanwil,nama_cabang asc
				LIMIT_OFFSET
				";
				
	$filter = array();
	if($kanwil)
	{
		$sql = str_replace('KANWIL_FLAG',' AND a.id_m_kanwil = ?',$sql);
		array_push($filter,$kanwil);
	}
	else {
		$sql = str_replace('KANWIL_FLAG',' AND a.id_m_kanwil != 4 ',$sql);
	}
	if($unit_kerja)
	{
		$sql = str_replace('UNIT_FLAG',' AND a.id_m_cabang = ?',$sql);
		array_push($filter,$unit_kerja);
	}else
		$sql = str_replace('UNIT_FLAG',' ',$sql);
		
	$sql = str_replace('PRODUCT_FLAG',$product[$type],$sql);
	if($limit !=0)
	{
		$sql = str_replace('LIMIT_OFFSET',' LIMIT ? OFFSET ? ',$sql);
		array_push($filter,$limit);
		array_push($filter,$offset);
	}else
		$sql = str_replace('LIMIT_OFFSET',' ',$sql);
	return $this->db->query($sql,$filter);
	}
	
	public function get_produk($limit=0,$offset=0,$type=0,$id_cabang='',$tanggal)
	{
		$product = array('',' and a.id_m_produk != 15',' and a.id_m_produk = 15');
		$sql ="	SELECT
					MAX(a.id_m_cabang) as id_m_cabang,
					MAX(b.nama_cabang) as nama_cabang,
					a.id_m_produk,
					MAX(nama_produk) as nama_produk,
					SUM(a.volume) as volume,
					SUM (a.ijp_accrual) as ijp					
				FROM t_pencapaian_vol_ijp a 
				JOIN m_cabang b on a.id_m_cabang = b.id_m_cabang 
				JOIN m_produk c on a.id_m_produk = c.id_m_produk
				WHERE 
					a.tanggal = '{$tanggal}'
					PRODUCT_FLAG
					BRANCH_FLAG
				GROUP BY a.id_m_produk
				ORDER BY nama_produk
				LIMIT_OFFSET
			";
			
		$sql = str_replace('PRODUCT_FLAG',$product[$type],$sql);
		$filter = array();
		
		if($id_cabang){
			$sql = str_replace('BRANCH_FLAG',' AND a.id_m_cabang = ? ',$sql);
			array_push($filter,$id_cabang);
		}else
			$sql = str_replace('BRANCH_FLAG','',$sql);
			
		if($limit !=0 || $offset !=0)
		{
			$sql = str_replace('LIMIT_OFFSET',' LIMIT ? OFFSET ? ',$sql);
			array_push($filter,$limit);
			array_push($filter,$offset);
		}else
			$sql = str_replace('LIMIT_OFFSET',' ',$sql);
			
		return $this->db->query($sql,$filter);	
	}
	
	public function get_rkap($limit=0,$offset=0,$tahun,$kanwil='',$cabang='',$produk='')
	{
		$sql = "SELECT 
					c.id_m_kanwil,
					c.nama_kanwil,
					b.id_m_cabang,
					b.nama_cabang,
					d.id_m_produk,
					d.nama_produk,
					a.flag_syariah,
					a.volume as volume,
					a.ijp_accrual as ijp 
				FROM 
					t_rkap_vol_ijp a join m_cabang b ON a.id_m_cabang = b.id_m_cabang 
				JOIN m_kanwil c on b.id_m_kanwil = c.id_m_kanwil
				JOIN m_produk d on a.id_m_produk = d.id_m_produk
				WHERE
					a.tahun = ?
					KANWIL_FLAG
					BRANCH_FLAG
					PRODUCT_FLAG
				ORDER BY c.id_m_kanwil, b.id_m_cabang,d.id_m_produk,a.flag_syariah
					LIMIT_OFFSET
			";
		$filter = array();
		array_push($filter,$tahun);
		if($kanwil){
			$sql = str_replace('KANWIL_FLAG',' AND c.id_m_kanwil = ? ',$sql);
			array_push($filter,$kanwil);
		}else
			$sql = str_replace('KANWIL_FLAG','',$sql);
			
		if($cabang){
			$sql = str_replace('BRANCH_FLAG',' AND b.id_m_cabang = ? ',$sql);
			array_push($filter,$cabang);
		}else
			$sql = str_replace('BRANCH_FLAG','',$sql);
		
		if($produk){
			$sql = str_replace('PRODUCT_FLAG',' AND d.id_m_produk = ? ',$sql);
			array_push($filter,$produk);
		}else
			$sql = str_replace('PRODUCT_FLAG','',$sql);	
		
		if($limit !=0)
		{
			$sql = str_replace('LIMIT_OFFSET',' LIMIT ? OFFSET ? ',$sql);
			array_push($filter,$limit);
			array_push($filter,$offset);
		}else
			$sql = str_replace('LIMIT_OFFSET',' ',$sql);
		
		return $this->db->query($sql,$filter);		
	}
	
	// get periode
	public function get_periode($tanggal){
		$sql = "SELECT * from t_periode where to_date(to_char(tgl_periode,'YYYY-MM-DD'),'YYYY-MM-DD') = '{$tanggal}' AND id_periode = 1";
		return $this->db->query($sql);
	}
	
	// check rkap yang sudah ada
	public function check_rkap($id_cabang,$tahun)
	{
		$sql = "select id_m_cabang from t_rkap_vol_ijp where id_m_cabang = {$id_cabang} AND tahun = {$tahun} group by id_m_cabang";
		$result = $this->db->query($sql)->row();
		if($result)
			return false;
		else
			return true;
	}
	public function cabang_not_rkap($tahun){
		$sql = "SELECT *
				FROM m_cabang 
				WHERE id_m_cabang NOT IN (
						select 
							id_m_cabang 
						from t_rkap_vol_ijp 
						where tahun = {$tahun} group by id_m_cabang
						) 
					AND id_m_kanwil != 4
		";	
		return $this->db->query($sql);
	}
	
	function get_total_record($tgl){
		$sql = "select id_m_cabang from t_pencapaian_vol_ijp where tanggal = '{$tgl}'";
		return $this->db->query($sql)->num_rows();
	}
}