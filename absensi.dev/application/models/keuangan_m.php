<?php 
class keuangan_m extends CI_Model{

	public function get_akun($limit=0,$offset=0,$where='')
	{
		$sql = 'SELECT 
					a.bulan,
					a.tahun,
					a.nominal,
					b.id_m_akun,
					b.nama_akun 
				FROM t_posisi_keuangan a 
				JOIN m_akun b ON a.id_m_akun = b.id_m_akun ';
		if($where)
			$sql .= " WHERE {$where}";
		if($limit !=0)
			$sql .= " LIMIT {$limit} OFFSET {$offset} ";
		return $this->db->query($sql);	
	}
	
	public function inserted_batch($table,$data){
		$this->db->trans_start();
			$this->db->truncate($table);
			$this->db->insert_batch($table,$data);				
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}
	
	
}