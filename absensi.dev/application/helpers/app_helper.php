<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

// helper for autentification user
function checkACL($type = 1, $url = "") {
	if (!is_int($type) || $type < 1) show_error('Parameter ACL tidak valid.');

	$CI = get_instance();
	$db = $CI->db;
	$id_dd_kelompok = $CI->session->userdata('id_m_kelompok');
	
	if (empty($url)) $url = getIndexPage();
	
	$sql = "SELECT b.permission FROM m_menu a INNER JOIN m_acl b ON b.id_m_menu=a.id_m_menu WHERE lower(a.link) = ? AND b.id_m_kelompok = ? ";
	$row = $db->query($sql, array(strtolower($url), $id_dd_kelompok))->row();
	
	if(!empty($row))
	{
		$cek = $row->permission & $type;
		return ($cek === $type);
	}		
	else
		return FALSE;
}

// konversi segmen ketiga jadi index, karena yg disimpan di database adalah indexnya
function getIndexPage() {
	$CI = get_instance();
	$a1 = $CI->uri->segment(1, "main");
	$a2 = $CI->uri->segment(2);
	//$a3 = "index";
	//return "$a1/$a2/$a3";
	if($a2)
		return "$a1/$a2";
	else	
		return "$a1";
}

function checkAllACL($url = "") {
	$CI = get_instance();
	$db = $CI->adodb->getConnection();
	$id_dd_kelompok = $CI->session->userdata('id_dd_kelompok');

	if (empty($url)) $url = getIndexPage();

	$arrConstACL = $CI->config->item('ACL_CONST');
	arsort($arrConstACL, SORT_NUMERIC);

	$sql = "SELECT b.permission FROM dd_menu a INNER JOIN dd_acl b ON b.id_dd_menu=a.id_dd_menu WHERE lower(a.url) = ? AND b.id_dd_kelompok = ?";
	$permission = $db->GetOne($sql, array(strtolower($url), $id_dd_kelompok));

	$ret = array();
	foreach($arrConstACL as $v) $ret[$v] = (($permission & $v) === $v);

	return $ret;
}

// menu 
function side_menu(){
	$CI = get_instance();
	$CI->load->model('menu_m');
	$id_m_kelompok = $CI->session->userdata('id_m_kelompok');
	//$id_m_kelompok = 1;
	$menu 		= $CI->menu_m->get_menu($id_m_kelompok)->result();
	$sub_menu 	= $CI->menu_m->sub_menu($id_m_kelompok)->result();
	
	$data = array();
	$sub_arr = array();
	foreach($sub_menu as $sub)
	{
		$sub_arr[$sub->parent][] = array('sub_menu'=>$sub->nama_menu,'url'=>$sub->link,'icon'=>$sub->icon);
	}
	
	foreach($menu as $mn)
	{
		$menu_sub = array();
		if(array_key_exists($mn->id_m_menu,$sub_arr))
			$menu_sub = $sub_arr[$mn->id_m_menu];
			
		$data[] = array('id_m_menu'=>$mn->id_m_menu,
						'nama_menu'=>$mn->nama_menu,
						'link'=>$mn->link,
						'sub_menu' => $menu_sub,
						'icon'=>$mn->icon);
						
	}

	return $data;
}

function menu(){
	$CI = get_instance();
	$CI->load->model('user_m');
	$id = $CI->session->userdata('id_m_kelompok');
	$parent = $CI->user_m->get_parent_menu($id)->result();
	
	$menus = array();
	$j = 0;
	foreach($parent as $row)
	{
		$subs = $CI->user_m->get_menu_by_parent($row->id_m_menu,$id)->result();
		$i=0;
		
		$sub_arr = array();
		foreach($subs as $sub)
		{
			$sub_arr[$j]['menu']	= $sub->nama_menu;
			$sub_arr[$j]['url'] 	= $sub->link;
			$sub_arr[$j]['icon']	= $sub->icon;
			$sub_arr[$j]['title']	= $sub->title;
			$sub_arr[$j]['id']	= $sub->id_m_menu;
			$i++;
		}	
		
		$menus[$j]['menu']	= $row->nama_menu;
		$menus[$j]['url'] 	= $row->link;
		$menus[$j]['icon']	= $row->icon;
		$menus[$j]['title']	= $row->title;
		$menus[$j]['id']	= $row->id_m_menu;
		$menus[$j]['subs']	= $sub_arr;
		$j++;
	}
	
	return $menus;
}
function actived(){
	$CI = get_instance();
	$CI->load->model('menu_m');
	$url = $CI->uri->segment(1);
	$segmen2 = $CI->uri->segment(2);
	if(!is_null($segmen2))
		$url = $url.'/'.$segmen2;
	
	$row = $CI->menu_m->menu_by_url(strtolower($url))->row();
	if($row)
		return array('menu'=> $row->menu,'sub_menu'=>$row->sub_menu);
	else
		return false;
}
/* End of file app_helper.php */
/* Location: ./system/application/helpers/app_helper.php */