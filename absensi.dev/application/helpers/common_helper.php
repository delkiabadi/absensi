<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('uang'))
{
    function uang($jumlah,$decimal = false)
    {
        if ($decimal)
			return number_format($jumlah, 2, ",", ".");
		else
			return number_format($jumlah, 0, ",", ".");
    }   
}