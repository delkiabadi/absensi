<?php
class Operasional_db{
	var $CI;
	var $DB;
	public function __construct(){
		$this->CI = &get_instance();
	}
	
	public function connect(){

		$this->DB	= $this->CI->load->database('operasional',TRUE);
		$conected 	= $this->DB->initialize();
		if($conected)
		{
			return true;
		}
		else
		{ 
			return false;
		}							
	}
	
	public function ijp_kupedes(){
		// ADD 10 APRIL 2015 FOR KUPEDES 
		// IJP KUPEDES DIMASUKKAN KE PRODUK MIKRO
		$kup_mulai = '2015-01-01';
		$kup_akhir = date('Y-m-d');
		$tgl_start_kupedes 	= strtotime($kup_mulai);
		$tgl_end_kupedes	= strtotime($kup_akhir);
		
		$jum_bulan = (int) date('n', $tgl_end_kupedes) - (int)date('n', $tgl_start_kupedes) + (((int)date('Y', $tgl_end_kupedes)-(int)date('Y', $tgl_start_kupedes))*12);
		$dataArrKup = Array();

		for ($b=1; $b<=$jum_bulan+1; $b++)
		{
			$newdate = date("Y-m-d",$this->add_date($tgl_start_kupedes, $b));
			$tgl_cari_new = strtotime ( '-1 day' , strtotime($newdate) );
			$tgl_cari = date('Y-m-d',$tgl_cari_new);
			$sql = "DECLARE @tgl_cari DATETIME
							DECLARE @tgl_start DATETIME
							SET @tgl_start = ?
							SET @tgl_cari = ?
							SELECT
								a.id_dc_wilayah_kerja,
								sum(case when tgl_klaim is null then (pokok_pembiayaan) else
								(case when month(tgl_klaim)= month(@tgl_cari) then 0 else (pokok_pembiayaan) end)  end) as pokok_kredit,
								sum(case when tgl_klaim is null then (nilai_ijp)else 
								(case when month(tgl_klaim)= month(@tgl_cari) then 0 else (nilai_ijp) end) end)as total_ijp,
								sum(case when tgl_klaim is null then (nilai_ijp/jw_tahun)else 
								(case when month(tgl_klaim)= month(@tgl_cari) then 0 else (nilai_ijp/jw_tahun)end) end) as ijp_tahunan,
								sum (case
								-- 1. bila pada bulan yang dicari sudah klaim 
								WHEN tgl_klaim IS NOT NULL AND DATEDIFF(MONTH, tgl_klaim, @tgl_cari)=0	AND jw_tahun > 1
										THEN (nilai_ijp/jw_tahun) * ((DATEDIFF(month, tgl_klaim, jatuh_tempo2))/12)
								-- 2. jika sudah klaim, ijp tidak dihitung (di nol-kan)
								WHEN DATEDIFF(MONTH, tgl_klaim, @tgl_cari)>=0 THEN 0 
								-- 3. jika sudah jatuh_tempo ijp di nol kan
								WHEN DATEDIFF(MONTH, jatuh_tempo, @tgl_cari)>=0 THEN 0 
								-- 4.meNol kan ijp yang tidak sedang berulang tahun
								WHEN bulan != MONTH (@tgl_cari)then 0
								-- 5. selainnya, cukup dibagi dengan jangka waktunya (max 48)
										ELSE nilai_ijp/jw_tahun
								END) as ijp_acrual
							FROM opmt_acrual_tahunan_v a
							INNER JOIN dc_wilayah_kerja wil on wil.id_dc_wilayah_kerja = a.id_dc_wilayah_kerja
							INNER JOIN dd_bank_cabang_master c
								ON c.id_dd_bank_cabang=a.id_dd_bank_cabang and c.id_dc_wilayah_kerja = a.id_dc_wilayah_kerja
							INNER JOIN dd_bank d ON d.id_dd_bank=c.id_dd_bank
							WHERE
								tgl_sk between @tgl_start and @tgl_cari
								and a.id_dc_produk = 4
							GROUP BY a.id_dc_wilayah_kerja
							ORDER BY a.id_dc_wilayah_kerja			
						"; 
			$rows = $this->DB->query($sql,array($kup_mulai,$tgl_cari));
			foreach($rows->result() as $row)
			{	
				$dataArrKup[$b][$row->id_dc_wilayah_kerja] = $row->ijp_acrual;
			}
		}
		// total kupedes per cabang
		$totalKupedes = array();
		$check = 0;
		foreach($dataArrKup as $row)
		{
			
				foreach($row as $key=>$ijp_kupedes)
				{
					if($check == 0)
						$totalKupedes[$key] = $ijp_kupedes;
					else 
						$totalKupedes[$key] += $ijp_kupedes;
				}
				$check =1;
		}
		// END KUPEDES 
		// RETURN Array
		// FORMAT DATA : Array([id_cabang] => $nilai_ijp_kupedes,...)
		return $totalKupedes;
	}
	
	// GET DATA IJP EKSISTING
	function ijp_eksisting(){
		$tgl_awal	= '2011-01-01';		// awal dari akrualisasi
		$tgl_cari	= date('Y-m-d');	// tgl pencarian
		$tgl_cari1	= '2014-12-31';     // akhir periode 2014
		
		$sql = "
				DECLARE @tgl_awal DATETIME
				DECLARE @tgl_cari DATETIME
				DECLARE @tgl_cari1 DATETIME
				SET @tgl_awal = ?
				SET @tgl_cari = ?
				SET @tgl_cari1 = ?
				
				SELECT
					a.id_dc_wilayah_kerja, a.id_dc_produk,
					(a.ijp_acrual - isnull(a1.ijp_acrual,0)) as ijp_accrual
				FROM
				--query ijp acrual untuk penerbitan sp dari awal (01-01-2011) sampe sekarang
				(
					SELECT
						a.id_dc_wilayah_kerja, a.id_dc_produk,
						SUM (CASE
								-- 1. jika belum terbit, dinolkan
								WHEN DATEDIFF(MONTH, tgl_sk, @tgl_cari)< 0 THEN 0
								
								-- 1. jika sudah klaim,  masukin semua ijp
								WHEN  DATEDIFF(MONTH, tgl_klaim, @tgl_cari)>=0 THEN nilai_ijp
				
								-- 2. jika sudah jatuh tempo,  masukin semua ijp
								WHEN DATEDIFF(MONTH, jatuh_tempo, @tgl_cari)>=0 THEN nilai_ijp
				
								-- 3. untuk yang lain, ijp dihitung berdasarkan selisih tgl realisasi dengan tgl cari (karena ijp dihitung dari tgl realisasi)
								ELSE nilai_ijp * (( 1 + DATEDIFF(MONTH, tgl_realisasi, @tgl_cari))/jangka_waktu_48)
							END) as ijp_acrual
					FROM 
						opmt_transaksi_acrual_v a
					LEFT JOIN (
								SELECT 
									a.id_opmt_transaksi_penjaminan,
									a.id_dc_wilayah_kerja,
									(a.nilai_ijp - ISNULL(premi_jiwasraya, 0)) as nilai_ijp
								FROM opmt_pembayaran_ijp_master a
								LEFT JOIN 
									opmt_pnm_master b
									ON b.id_opmt_transaksi_penjaminan=a.id_opmt_transaksi_penjaminan AND a.id_dc_wilayah_kerja=b.id_dc_wilayah_kerja
								)b ON a.id_opmt_transaksi_penjaminan=b.id_opmt_transaksi_penjaminan	AND a.id_dc_wilayah_kerja=b.id_dc_wilayah_kerja
					INNER JOIN dd_bank_cabang_master c
						ON c.id_dd_bank_cabang=a.id_dd_bank_cabang AND a.id_dc_wilayah_kerja=c.id_dc_wilayah_kerja
					INNER JOIN dd_bank d
						ON d.id_dd_bank=c.id_dd_bank
					WHERE
						tgl_sk between @tgl_awal and @tgl_cari
						AND a.id_dc_produk <> 15
						GROUP BY a.id_dc_wilayah_kerja, a.id_dc_produk
					)a
					LEFT JOIN
						--query ijp acrual untuk penerbitan sp dari awal (01-01-2011) sampe tahun terpilih-1
						(
							SELECT
								a.id_dc_wilayah_kerja, a.id_dc_produk,
								SUM (CASE
										-- 1. jika belum terbit, dinolkan
											WHEN DATEDIFF(MONTH, tgl_sk, @tgl_cari1)< 0 THEN 0
				
										-- 1. jika sudah klaim,  masukin semua ijp
										WHEN  DATEDIFF(MONTH, tgl_klaim, @tgl_cari1)>=0 THEN nilai_ijp
				
										-- 2. jika sudah jatuh tempo,  masukin semua ijp
										WHEN DATEDIFF(MONTH, jatuh_tempo, @tgl_cari1)>=0 THEN nilai_ijp
				
										-- 3. untuk yang lain, ijp dihitung berdasarkan selisih tgl realisasi dengan tgl cari (karena ijp dihitung dari tgl realisasi)
										ELSE nilai_ijp * (( 1 + DATEDIFF(MONTH, tgl_realisasi, @tgl_cari1))/jangka_waktu_48)
									END) as ijp_acrual
								FROM opmt_transaksi_acrual_v a
					LEFT JOIN (
						SELECT a.id_opmt_transaksi_penjaminan,
							a.id_dc_wilayah_kerja,
							(a.nilai_ijp - ISNULL(premi_jiwasraya, 0)) as nilai_ijp
						FROM opmt_pembayaran_ijp_master a
						LEFT JOIN opmt_pnm_master b
							ON b.id_opmt_transaksi_penjaminan=a.id_opmt_transaksi_penjaminan AND a.id_dc_wilayah_kerja=b.id_dc_wilayah_kerja
						)b ON a.id_opmt_transaksi_penjaminan=b.id_opmt_transaksi_penjaminan	AND a.id_dc_wilayah_kerja=b.id_dc_wilayah_kerja
			
					INNER JOIN dd_bank_cabang_master c
						ON c.id_dd_bank_cabang=a.id_dd_bank_cabang AND a.id_dc_wilayah_kerja=c.id_dc_wilayah_kerja
					INNER JOIN dd_bank d
						ON d.id_dd_bank=c.id_dd_bank
			
					WHERE
						tgl_sk between @tgl_awal and @tgl_cari
						AND a.id_dc_produk <> 15
						GROUP BY a.id_dc_wilayah_kerja, a.id_dc_produk
				)a1 ON a.id_dc_wilayah_kerja=a1.id_dc_wilayah_kerja AND a.id_dc_produk=a1.id_dc_produk
				ORDER BY
					a.id_dc_wilayah_kerja, a.id_dc_produk";
		$rows = $this->DB->query($sql,array($tgl_awal,$tgl_cari,$tgl_cari1));
		$arrData = array();
		foreach($rows->result() as $row)
		{
			$arrData[$row->id_dc_wilayah_kerja][$row->id_dc_produk] = $row->ijp_accrual;
		}
		
		return $arrData;
		// END IJP EKSISTING	
	}
	
	// GET DATA IJP KUR
	function ijp_kur(){
		$tanggal = date('Y-m-d');	// tgl pencarian
		$bulan = date('n');	// bulan
		$tahun = date('Y');	// tahun
		
		$sql = "	DECLARE @tanggal DATETIME
					DECLARE @bulan INT
					DECLARE @tahun INT
					SET @tanggal = ?
					SET @bulan = ?
					SET @tahun = ?
					
					SELECT
						a.id_dc_wilayah_kerja,
						SUM(a.imbal_jasa) AS ijp_accrual
					FROM
					(
						SELECT
							a.id_dc_wilayah_kerja,
							a.id_opmt_sertifikat,
							b.id_opmt_permohonan,
							c.id_opmt_transaksi_penjaminan,
							a.nomor_sk,
							c.flag_proses,
							c.pokok_pembiayaan,
							(d.nilai_ijp / CEILING(c.jangka_waktu / 12)) AS imbal_jasa,
							b.flag_syariah
						FROM
							opmt_sertifikat_master a
							INNER JOIN
							opmt_permohonan_master b ON a.id_opmt_sertifikat = b.id_opmt_sertifikat AND a.id_dc_wilayah_kerja = b.id_dc_wilayah_kerja
							INNER JOIN
							opmt_transaksi_penjaminan_master c ON b.id_opmt_permohonan = c.id_opmt_permohonan AND b.id_dc_wilayah_kerja = c.id_dc_wilayah_kerja
							INNER JOIN
							opmt_pembayaran_ijp_master d ON d.id_opmt_transaksi_penjaminan = c.id_opmt_transaksi_penjaminan AND d.id_dc_wilayah_kerja = c.id_dc_wilayah_kerja
					
						WHERE
							-- filter tanggal penerbitan
							a.tgl_sk <= @tanggal
							AND
							-- filter bulan penerbitan
							MONTH(a.tgl_sk) <= @bulan
							AND
							-- filter tahun penerbitan
							YEAR(a.tgl_sk) <= @tahun
							AND
							-- filter produk
							b.id_dc_produk = 15
							AND
							-- filter periode penagihan, disesuaikan dengan jangka waktu kredit
							CEILING(c.jangka_waktu/12) >= (@tahun - YEAR(a.tgl_sk) + 1)
					) a
					GROUP BY
						a.id_dc_wilayah_kerja
					ORDER BY
						a.id_dc_wilayah_kerja";
		$rows = $this->DB->query($sql,array($tanggal,$bulan,$tahun));
		$dataArr = array();
		foreach($rows->result() as $row){
			$dataArr[$row->id_dc_wilayah_kerja] = $row->ijp_accrual;
		}
		
		return $dataArr;
		// END IJP KUR
	}
	
	// VOLUME IJP ACCRUALL
	public function volume_ijp(){
		$awal	= '2015-01-01';	// tgl awal periode
		$akhir	= date('Y-m-d');	// tgl pencarian
		
		$sql = 	"SELECT
					a.id_dc_wilayah_kerja,
					d.grouping_produk as id_dc_produk, 
					SUM(c.pokok_pembiayaan) AS volume
				FROM
					opmt_sertifikat_master a
				INNER JOIN
					opmt_permohonan_master b ON a.id_opmt_sertifikat = b.id_opmt_sertifikat AND a.id_dc_wilayah_kerja = b.id_dc_wilayah_kerja
				INNER JOIN
					opmt_transaksi_penjaminan_master c ON b.id_opmt_permohonan = c.id_opmt_permohonan AND b.id_dc_wilayah_kerja = c.id_dc_wilayah_kerja
				INNER JOIN dc_produk d ON d.id_dc_produk=b.id_dc_produk
				WHERE
					a.tgl_sk BETWEEN ? AND ?
				GROUP BY
					a.id_dc_wilayah_kerja, d.grouping_produk 
				ORDER BY
					a.id_dc_wilayah_kerja, d.grouping_produk 
				";
		$rows		= $this->DB->query($sql,array($awal,$akhir));
		$dataArr 	= array();
		foreach($rows->result() as $row)
		{
			$dataArr[$row->id_dc_wilayah_kerja][$row->id_dc_produk] = $row->volume;
		}
		return $dataArr;	
	}
	
	// fungsi tambah tanggal 
	public function add_date($orgDate,$mth)
	{ 
		$cd = $orgDate;
		$retDAY = mktime(0,0,0,date('m',$cd)+$mth,1,date('Y',$cd)); 	//karena yang dipakai hanya bulan dan tahun, tanggal diset 1 (untuk menghindari error jika tgl 30/31)
		return $retDAY; 
	}
	
	public function get_error_db(){
		$error = $this->DB->error();  
		return $error['message'];
	}
}
