<?php
class Akuntansi{
	var $CI;
	var $DB;
	public function __construct(){
		$this->CI = &get_instance();
	}
	public function get_con()
	{
		return "tes";
	}
	public function connect(){

		$this->DB	= $this->CI->load->database('akuntansi',TRUE);
		$conected 	= $this->DB->initialize();
		if($conected)
		{
			return true;
		}
		else
		{ 
			return false;
		}							
	}
	
	public function get_saldo($produk,$wil,$params=array())
	{
		$sql =" SELECT 
					e.id_dd_wilayah_kerja,
					b.coa_number,				
					b.coa_uraian AS uraian,
					(CASE WHEN (c.flag & 1048576) = 1048576 THEN 'k' ELSE 'd' END) AS acc_type,
					a.akhir
				FROM akmt_saldo a
				JOIN akdd_coa_detail_v b ON a.id_akdd_coa = b.id_akdd_coa AND a.id_dd_wilayah_kerja = b.id_dd_wilayah_kerja
				JOIN akdd_klasifikasi_coa c ON b.id_akdd_klasifikasi_coa = c.id_akdd_klasifikasi_coa
				JOIN akmt_periode d ON a.id_akmt_periode = d.id_akmt_periode AND a.id_dd_wilayah_kerja = d.id_dd_wilayah_kerja
				JOIN ( 
					SELECT 
					max(a.id_akmt_saldo) AS id_akmt_saldo, 
					c.tahun, 
					c.bulan,
					c.id_dd_wilayah_kerja
					FROM 
					akmt_saldo a
					JOIN akdd_coa b ON a.id_akdd_coa = b.id_akdd_coa AND a.id_dd_wilayah_kerja = b.id_dd_wilayah_kerja
					JOIN akmt_periode c ON a.id_akmt_periode = c.id_akmt_periode AND a.id_dd_wilayah_kerja = c.id_dd_wilayah_kerja
					WHERE 
					b.id_akdd_klasifikasi_coa = ANY (ARRAY[4, 5])
					AND
					substr(b.coa_number, 10, 1) IN ({$sql_produk}, ?)
					AND
					c.bulan = ?
					AND
					c.tahun = ?
					{$sql_wilayah}
					GROUP BY 
					b.id_akdd_coa, 
					c.tahun, 
					c.bulan,
					c.id_dd_wilayah_kerja
				) e ON a.id_akmt_saldo = e.id_akmt_saldo AND e.bulan = d.bulan AND e.tahun = d.tahun AND a.id_dd_wilayah_kerja = e.id_dd_wilayah_kerja
				ORDER BY
				b.coa_number,
				e.id_dd_wilayah_kerja
			";
		return $this->DB->query($sql,$params);	
	}
	
	
	public function get_coa_detail($produk,$sql_wilayah,$params=array()){
		$sql =	"
				SELECT
					a.*
				FROM
					(
						SELECT
							id_akdd_coa_m,
							id_akdd_coa_m_ref,
							0::INT AS id_dd_wilayah_kerja,
							coa_number,
							coa_uraian,
							level_number,
							level_length
						FROM
							akdd_coa_m_v
						WHERE
							substr(coa_number, 10, 1) IN ({$produk}, ?)
							AND							
							level_number <= ?
							AND
							id_akdd_klasifikasi_coa = ?
						UNION
						SELECT
							id_akdd_coa,
							id_akdd_coa_ref,
							id_dd_wilayah_kerja,
							coa_number,
							coa_uraian,
							level_number,
							level_length
						FROM
							akdd_coa_v
						WHERE
							substr(coa_number, 10, 1) IN ({$produk}, ?)
							AND							
							level_number IS NULL									
							AND
							id_akdd_klasifikasi_coa = ?
							{$sql_wilayah}
					) a
				ORDER BY
					a.coa_number,
					a.id_dd_wilayah_kerja
				";
		return $this->DB->query($sql,$params);			
	}
	
	public function get_rows(){
		return $this->DB->query('select * from dd_wilayah_kerja');
	}
	public function get_where($table,$select,$where){
		return $this->DB->query("select {$select} FROM {$table} where {$where} ");
	}
}
