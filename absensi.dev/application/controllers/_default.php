<?php 
class _Default extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('common_m');
		$this->load->helper('app');
		$this->load->library('operasional_db');
	}
	
	public function acl(){
		$result 	= TRUE;
		$message	= '';
		try{
			$menus 		= $this->common_m->get_where('m_menu','*',' 1=1 and parent !=0 ')->result();
			$kelompok	= $this->common_m->get_where('m_kelompok','*','1=1')->result();
			$data 		= array();
			
			//mengosongkan tabel acl
			$this->common_m->sql_query('truncate table m_acl restart identity');
			
			$arrConstACL = $this->config->item('ACL_CONST');
			
			// setiap kelompok punya semua menu
			//foreach($menus as $menu){
				foreach($kelompok as $kel)
				{
						$input = array(	'id_m_menu'=>$menu->id_m_menu,
									'id_m_kelompok'=>$kel->id_m_kelompok,
									'permission'=>0);
						array_push($data,$input);												
				}
			//}
			if(!$this->common_m->inserted_batch('m_acl',$data))
				THROW NEW exception('GAGAL INSERT ACL');
			
			if(!$this->common_m->sql_query('update m_acl set permission = 15 where id_m_kelompok = 1 and id_m_menu >=14'))
				THROW NEW exception('GAGAL INISIALISASI ACL');
			
			$message = 'proses sukses!';	
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
		
		echo json_encode(array('status'=>$result,'message'=>$message));	
	}
	public function posisi_keuangan(){
		$this->common_m->sql_query('truncate table t_posisi_keuangan restart identity');
		$this->common_m->sql_query('truncate table t_posisi_keuangan_rkap restart identity');
	}
	
	public function status_notif(){
		$this->common_m->sql_query('truncate table t_status restart identity');
		$this->common_m->sql_query('truncate table t_notification restart identity');
	}
	
	public function list_menu(){
		$menu = side_menu();
		print_r($menu);
	}
	
	public function tes()
	{
		$row = $this->common_m->sql_query('select * from m_user_log where id_m_user_log = 31')->row();
		
		$now = strtotime(date('Y-m-d H:i:s'));
		$login_time = strtotime($row->login_time);
		$selisih = $now - $login_time;
		$jam = (($selisih % 604800)%86400)/60;
		
		print_r($jam);
		
		//$cek = 7 & 8;
		//print_r($this->uri->segment(1));
	}
	
	public function akuntansi(){
		$dbs = $this->load->database('tes',TRUE);
		$sub = 'select * from t_rkap_subrogasi';
		$klaim = 'select * from t_rkap_klaim';
		$inv	= 'select * from t_rkap_investasi';
		$subs 	= $dbs->query($sub);
		$klaims = $dbs->query($klaim);
		$invs 	= $dbs->query($inv);
		
		// truncate
		$this->db->query('truncate table t_rkap_subrogasi restart identity');
		$this->db->query('truncate table t_rkap_klaim restart identity');
		$this->db->query('truncate table t_rkap_investasi restart identity');
		
		$sub_arr = array();
		$klm_arr = array();
		$inv_arr = array();
		
		foreach($subs->result() as $s){
			$data = array();
			$data['id_m_cabang'] = $s->id_m_cabang;
			$data['subrogasi'] = $s->subrogasi;
			$data['flag_program']	= $s->flag_program;
			$data['tahun']		= 2015;
			array_push($sub_arr,$data);
		}
		
		foreach($klaims->result() as $k){
			$data = array();
			$data['id_m_cabang'] = $k->id_m_cabang;
			$data['klaim'] = $k->klaim;
			$data['flag_program']	= $k->flag_program;
			$data['tahun']		= 2015;
			array_push($klm_arr,$data);
		}
		
		foreach($invs->result() as $i){
			$data = array();
			$data['id_m_cabang'] 	= $i->id_m_cabang;
			$data['investasi'] 		= $i->investasi;
			$data['flag_program']	= $i->flag_program;
			$data['flag_syariah']	= $i->flag_syariah;
			$data['tahun']			= 2015;
			array_push($inv_arr,$data);
		}
		
		$this->db->insert_batch('t_rkap_subrogasi',$sub_arr);
		$this->db->insert_batch('t_rkap_investasi',$inv_arr);
		$this->db->insert_batch('t_rkap_klaim',$klm_arr);
	}
	
	public function keuangan_rkap(){
		$dbs = $this->load->database('tes',TRUE);
		$laba_rugi = $dbs->query('select * from t_laba_rugi_rkap');
		$keuangan = $dbs->query('select * from m_posisi_keuangan');
		
		$laba_arr = array();
		$keua_arr = array();
		foreach($laba_rugi->result() as $lb)
		{
			$data = array('tahun'=>$lb->tahun,
						  'id_m_akun'=>$lb->id_m_akun,
						  'nominal'=>$lb->jumlah);
			array_push($laba_arr,$data);			  
		}
		
		foreach($keuangan->result() as $k)
		{
			$data = array('tahun'=>$k->tahun,
						  'id_m_akun'=>$k->id_m_akun,
						  'nominal'=>$k->nominal);
			array_push($keua_arr,$data);			  
		}
		
		$this->db->query('truncate table t_posisi_keuangan_rkap restart identity');
		$this->db->query('truncate table t_laba_rugi_rkap restart identity');
		
		$this->db->insert_batch('t_posisi_keuangan_rkap',$keua_arr);
		$this->db->insert_batch('t_laba_rugi_rkap',$laba_arr);
	}
	
	public function keuangan_realisasi(){
		$dbs = $this->load->database('tes',TRUE);
		$laba_rugi = $dbs->query('select * from t_laba_rugi');
		$keuangan = $dbs->query('select * from t_posisi_keuangan');
		
		$laba_arr = array();
		$keua_arr = array();
		foreach($laba_rugi->result() as $lb)
		{
			$data = array('bulan'=>$lb->bulan,
							'tahun'=>$lb->tahun,
						  'id_m_akun'=>$lb->id_m_akun,
						  'nominal'=>$lb->jumlah);
			array_push($laba_arr,$data);			  
		}
		
		foreach($keuangan->result() as $k)
		{
			$data = array(	'bulan'=>$k->bulan,
							'tahun'=>$k->tahun,
						  'id_m_akun'=>$k->id_m_akun,
						  'nominal'=>$k->nominal);
			array_push($keua_arr,$data);			  
		}
		
		$this->db->query('truncate table t_posisi_keuangan restart identity');
		$this->db->query('truncate table t_laba_rugi restart identity');
		
		$this->db->insert_batch('t_posisi_keuangan',$keua_arr);
		$this->db->insert_batch('t_laba_rugi',$laba_arr);
	}
	
	public function operasional_rkap(){
		$dbs = $this->load->database('tes',TRUE);
		$rows = $dbs->query('select * from t_rkap_vol_ijp');
		$vol = array();
		foreach($rows->result() as $row)
		{
			$data = array('tahun'=>2015,
						  'id_m_cabang'=>$row->id_m_cabang,
						  'id_m_produk'=>$row->id_m_produk,
						  'flag_syariah'=>$row->flag_syariah,
						  'volume'=>$row->volume,
						  'ijp_accrual'=>$row->ijp_accrual);
			array_push($vol,$data);			  
		}
		
		$this->db->query('truncate table t_rkap_vol_ijp restart identity');
		$this->db->insert_batch('t_rkap_vol_ijp',$vol);
		
	}
	
	function kosong(){
		$this->db->query('truncate table t_posisi_keuangan restart identity');
		$this->db->query('truncate table t_posisi_keuangan_rkap restart identity');
		$this->db->query('truncate table t_laba_rugi restart identity');
		$this->db->query('truncate table t_laba_rugi_rkap restart identity');
		
		$this->db->query('truncate table t_investasi restart identity');
		$this->db->query('truncate table t_rkap_investasi restart identity');
		$this->db->query('truncate table t_klaim restart identity');
		$this->db->query('truncate table t_rkap_klaim restart identity');
		$this->db->query('truncate table t_subrogasi restart identity');
		$this->db->query('truncate table t_rkap_subrogasi restart identity');
		
		$this->db->query('truncate table t_pencapaian_vol_ijp restart identity');
		$this->db->query('truncate table t_rkap_vol_ijp restart identity');
		
		$this->db->query('truncate table t_notif restart identity');
		$this->db->query('truncate table t_status restart identity');
		
		// update tabel t_periode
		$now 		= mktime(0,0,0,date('m')-1); // angka 2,7,1 yang dicetak tebal bisa dirubah rubah
		$tgl 		= date('Y-m-d',$now);
		print_r($tgl);
		
		$this->db->query("update t_periode set tgl_periode = '{$tgl}' where id_periode = 1");
	}
	
	function tes2(){
		$operasional 	= new operasional_db();
		$con 			= $operasional->connect(); 
		$ijp_eksisting	= $operasional->ijp_eksisting();
		//$volume_ijp	= $operasional->volume_ijp();
		print_r($ijp_eksisting);
	}
	
	function acl_by_menu()
	{
		$id_m_menu = $this->uri->segment(3,0);
		$kelompok	= $this->common_m->get_where('m_kelompok','*','1=1')->result();
		$data 		= array();
		foreach($kelompok as $kel)
		{
			$input = array(	'id_m_menu'=>$id_m_menu,
							'id_m_kelompok'=>$kel->id_m_kelompok,
							'permission'=>0);
			array_push($data,$input);												
		}
		
		$this->common_m->inserted_batch('m_acl',$data);
	}
}