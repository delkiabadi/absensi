<?php 
class Absen extends MY_Controller{
	var $start_date = '';
	var $end_date = '';
	public function __construct(){
		parent::__construct();
		$this->load->library(array('excel','uploads'));
        $this->load->model(array('common_m','absensi_m'));
		$this->load->helper(array('date','pagination'));
		$list_for_month = listDayForMonth(date('n'),date('Y'),false);
		$this->start_date 	= $list_for_month[0];
		$this->end_date  		= end($list_for_month); 	
	}
	
	public function index($page='')
        {
			$limit = 10;
			$offset = $page =='' ? 0 : ($page-1)*$limit;
			$total = $this->absensi_m->list_absensi(0,0,$this->start_date,$this->end_date)->num_rows();	
            $rows = $this->absensi_m->list_absensi($limit,$offset,$this->start_date,$this->end_date);
			$data = array();
            $data['bulan'] = getListBulan();
            $data['cur_bulan'] 	= date('n');
            $data['cur_tahun'] 	= date('Y');
			$data['rows']		= $rows->result();
			$data['total']		= $total;
			$data['page']			= paging('absen/index',$page,$total,$limit);
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
			{
				$this->load->view('absen/partial_index',$data);
			}else
				$this->template->load('absen/index',$data);
	}
        
	function proses(){
            try{
				$bulan = $this->getVar('bulan');
				$tahun = $this->getVar('tahun');
                               
				$cek_periode = $this->common_m->get_where('t_periode','*'," bulan = '{$bulan}' AND tahun = '{$tahun}' ");
				if($cek_periode->num_rows() > 0)
					throw new Exception('PERIDOE ABSEN SUDAH PERNAH DIINPUT!');
				$data = $this->uploads->do_upload('file_upload');
                if(!$data['status'])
                    throw new Exception ($data['upload_file']);    
	
                $file_name = $data['upload_file']['file_name'];
				$records = $this->read($file_name);
				if(!$records)
                     throw new Exception ('gagal baca data');
                if(!$this->save($records,$bulan,$tahun))
                   throw new Exception ('gagal simpan data!');
                $message = 'sukses';
            }catch(Exception $e)
            {
                $message = $e->getMessage();
            }
            
            echo $message;
	}
	function read($file_name){
            try{
                $path='pub/uploads';
                
                $obj = PHPExcel_IOFactory::load($path.'/'.$file_name);
		$sheetCount = $obj->getSheetCount();
		
		$obj->setActiveSheetIndex(0);
		$maxRow = $obj->getActiveSheet()->getHighestRow();
		$maxColumn = $obj->getActiveSheet()->getHighestColumn();
		$sheet = $obj->getActiveSheet()->toArray(null,true,true,true);
		//print_r($maxRow.' - '.$maxColumn);
		$start_column = 'A';
		$start_row = 2;
		// delete row yang tidak perlu
		for($i=1;$i<$start_row;$i++)
			unset($sheet[$i]);	
		$pegawai 	= array();
		$tanggals	= array();
		$data 		= array();	
		foreach($sheet as $row => $columns) {
			$nama = trim(strtolower($columns['A']));
			$exp_tgl 	= explode(' ',$columns['B']);
			$tgl 		= trim($exp_tgl[0]);
			
			// GET SEMUA PEGAWAI
			if(!in_array($nama,$pegawai))
				array_push($pegawai,$nama);
			
			// GET SEMUA HARI	
			if(!in_array($tgl,$tanggals))
				array_push($tanggals,$tgl);
			/*
			// UBAH JAM DALAM FORMAT 24 
			if(trim(strtoupper($exp_tgl[2])) == 'PM')
			{
				$exp_jam = explode(":",$exp_tgl[1]);
				$jam_24 = ($exp_jam[0]) + 12;
				$jam = $jam_24.':'.$exp_jam[1].':'.$exp_jam[2];
				
			}else
				$jam = 	$exp_tgl[1];
			*/
			// SIMPAN ABSEN BERDASARKAN UNTUK SETIAP PEGAWAI
                        
                        $jam = date("H:i:s", strtotime($exp_tgl[1].' '.$exp_tgl[2]));
                       
			$data[$nama][$exp_tgl[0]][] = $jam;	
			
		}
		//print_r($data);die();
		// INISIALISASI RANGE WAKTU AWAL DAN AKHIR
		$start_time_1 	= date('H:i:s',strtotime('00:00:00'));
		$start_time_2	= date('H:i:s',strtotime('16:30:00'));
		//print_r($start_time_1.'-'.$start_time_2.' = ');
		//print_r($start_time_1.' '.$start_time_2);
		//print_r($data);
		// AMBIL DATA BERDASARKAN NAMA PEGAWAI
		$data_arr = array();
		foreach($pegawai as $peg)
		{
				$data_absen = $data[$peg];
				foreach($data_absen as $tgl=>$jams)
				{
					$datang = null;
					$pulang = null;
                    $tgl = date('Y-m-d',strtotime($tgl));
					
					// JIKA ITU HARI SABTU MINGGU 
					// AMBIL WAKTU ABSEN TERCEPAT DAN TERLAMA
						$day = date('l', strtotime($tgl));
					if(strtolower($day) == 'saturday' || strtolower($day) == 'sunday'){
						$list_jam = array();
						foreach($jams as $waktu)
						{
							array_push($list_jam,$waktu);
						}
						// URUTKAN JAM DARI TERKECI KE TERBESAR
							sort($list_jam);	
							if(count($list_jam) > 0)
								$datang = $list_jam[0];
							if(count($list_jam) > 1)
								$pulang = end($list_jam);
						 
					}else
					{
						foreach($jams as $waktu)
						{							
							if($waktu >= $start_time_1 AND $waktu < $start_time_2)
							{
								if($datang !=null)
									$datang = ($datang > $waktu) ? $waktu : $datang;
								else
									$datang = $waktu;
							}else
							{
								if($pulang != null)
									$pulang = ($waktu > $pulang) ? $waktu : $pulang;
								else
									$pulang = $waktu; 
							}						
						}
					}
					$data_arr[$peg][$tgl]['datang'] = !is_null($datang) ? $datang : null;
					$data_arr[$peg][$tgl]['pulang'] = !is_null($pulang) ? $pulang : null;
						
				}	
			
		}
		
		return $data_arr;
            }  catch (Exception $e){
                return false;
            }
	}
        
        public function save($data,$bulan,$tahun){
           try{
            $insert = array();
			
			$listDayForMonth = listDayForMonth($bulan,$tahun);
			
            foreach($data as $nama=>$val)
            {
                // CARI ID PEGAWAI
                $nama = addslashes($nama);
				$nama = str_replace("\'",'\"',$nama);
                $row = $this->common_m->get_where('m_pegawai','id_m_pegawai'," lower(nama_pegawai) = lower('{$nama}')");
				
                $id = ($row->num_rows() > 0) ? $row->row()->id_m_pegawai : 0;
                if($id !=0)
				{
					$tgls =array();
                    foreach($val as $tgl=>$jam)
                    {
						$tgl = 	date('Y-m-d',strtotime($tgl));
						$obj=array('id_m_pegawai'=>$id,'tanggal'=>$tgl,'jam_datang'=>$jam['datang'],'jam_pulang'=>$jam['pulang']);
						array_push($insert,$obj);
						array_push($tgls,$tgl);
                    }
					// CEK APAKAH ADA YANG TIDAK ABSEN PADA RENTANG HARI KERJA
					// JIKA ADA TETAP DIMASUKKAN KE DATABASE DENGAN JAM PULANG DAN DATANG IS NULL
					$not_absen = array_diff($listDayForMonth,$tgls);
					foreach($not_absen as $tgl)
					{
						$obj = 	array( 	'id_m_pegawai'	=> $id,
										'tanggal'		=> date('Y-m-d',strtotime($tgl)),
										'jam_datang'	=> null,
										'jam_pulang'	=> null		
								);
						array_push($insert,$obj);		
					}
				}
            }
            // INSERT
			
				$this->db->trans_start();
				if(!$this->common_m->inserted_batch('t_absensi',$insert))
					throw new Exception('gagal insert data absensi');
				if(!$this->common_m->inserted('t_periode',array('bulan'=>$bulan,'tahun'=>$tahun)))
					throw new Exception('gagal insert periode absen');
				$this->db->trans_complete();	
            return true;
           }catch(Exception $e){
               $message = $e->getMessage();
               return false;
           }
        }
       function sorts(){
			$list_ = listDayForMonth(8,2015);
			print_r($list_);
	
	   }
}