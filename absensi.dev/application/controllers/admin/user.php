<?php

class user extends MY_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('common_m');
		$this->load->model('user_m');
		$this->load->helper('pagination');
	}
	
	public function index($page=''){
		if (!checkACL(ACL_VIE)) show_error(ERROR_200);		
		// PAGING
			$limit = 10;
			$offset = $page =='' ? 0 : ($page-1)*$limit;	
		
		$res 	= $this->user_m->list_user();
		$total 	= $res->num_rows();
		$rows	= $this->user_m->list_user($limit,$offset);
			
		$data = array();
		$data['add']	= base_url().'admin/user/tambah';
		$data['hapus']	= 'admin/user/hapus';
		$data['print']	= 'admin/user/print';
		$data['page']	= paging('admin/user/index',$page,$total,$limit);
		$data['rows']	= $rows->result();
		$this->template->load('admin/user/index',$data);	
	}
	public function add(){
		if (!checkACL(ACL_ADD)) show_error(ERROR_200);

		$kelompok = $this->common_m->get_table('m_kelompok')->result();	
		$data = array();
		$data['kelompok'] = $kelompok;
		$this->template->load('admin/user/tambah',$data,FALSE);			
	}
	public function tambahAct(){
		if (!checkACL(ACL_ADD)) show_error(ERROR_200);
	
		$result = true;
		$message = '';
		try
		{
			$username = strtolower($this->getVar('username'));
			$password = $this->getVar('password');
			$c_password = $this->getVar('konfirmasi_password');
			
			if($password !== $c_password)
				throw new exception('Konfirmasi password tidak sesuai');
			
			// check ketersedian username
			$check = $this->common_m->get_where('m_user',' id_m_user', " lower(user_name) = '{$username}' ");
			if($check->num_rows() > 0)
				throw new exception(' Uername sudah terdaftar!');
			$data = array(	'user_name'		=> $username,
							'pass_word' 	=> md5($this->getVar('password')),
							'id_m_kelompok'	=> $this->getVar('id_m_kelompok'),
							'id_m_cabang'	=> 1,
							'npp'			=> $this->getVar('npp'),
							'flag_active'	=> $this->getVar('status')=='0' ? true : false,
						);
			$result = $this->common_m->inserted('m_user',$data);
			if(!$result)
				throw new exception('Gagal tambah user!');
			$message = "Proses Berhasil!";
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
			echo json_encode(array("status"=>true,"msg"=>$message));	
	}
	public function edit($id_user=''){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$user = $this->common_m->get_where('m_user','*'," id_m_user = {$id_user}")->row();
		$kelompok = $this->common_m->get_table('m_kelompok')->result();	
		$data = array();
		$data['kelompok'] 	= $kelompok;
		$data['row']		= $user;
		$this->template->load('admin/user/edit',$data,FALSE);			
	}
	public function editAct(){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$result = true;
		$message = '';
		try
		{
			$id_user = $this->getVar('id_user');
			$user = $this->common_m->get_where('m_user','*'," id_m_user = {$id_user}")->row();
			
			// cek apakah username diganti atau tidak
			// jika diganti check ketersedian username
			$username 	= strtolower($this->getVar('username'));
			$password	= $this->getVar('password');
			$c_password	= $this->getVar('konfirmasi_password');	
			if($username !== strtolower($user->user_name))
			{
				$check = $this->common_m->get_where('m_user',' id_m_user', " lower(user_name) = '{$username}' ");
				if($check->num_rows() > 0)
					throw new exception(' Username sudah terdaftar!');
			}					
			$data = array(	'user_name'		=>$username,
							'id_m_cabang'	=>1,
							'npp'			=> $this->getVar('npp'),
							'flag_active'	=> $this->getVar('status') == '0' ? true : false,
							'id_m_kelompok'	=> $this->getVar('id_m_kelompok')
							);
			 //check apakah password diganti
			if($password)
			{
				if($password !== $c_password)
					throw new exception('Konfirmasi password tidak sesuai');
				$data['pass_word'] = md5($password);	
			}			
			$result = $this->common_m->updated('m_user',$data,array('id_m_user'=>$id_user));
			if(!$result)
				throw new exception("Gagal update user");
			$message = "Proses Update Berhasil!";	
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));	
	}
	public function hapus(){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$result = true;
		try {
			$arrID = $this->getVar('usr', TRUE);
			$listID = '';
			foreach ($arrID as $id) {
				
				$listID .= $id.',';
			}
			$listID .= '-1';
			$result = $this->common_m->sql_query("delete from m_user where id_m_user in({$listID})");
			if(!$result)
				throw new exception('Gagal hapus user!');
			$message = 'Proses hapus berhasil!';
		} catch (Exception $e) {
			//Set error status from exception...
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));
	}
	public function cetak(){
		// 	DATA
			$rows	= $this->user_m->list_user(0,0)->result();

		// 	REPORT		
		$mainCols 		  = array();

		$arrCol 		  = array();
		$arrCol['title']  = 'NO.';
		$arrCol['width']  = 10;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'R';
		$arrCol['label'] = '1';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'MENU';
		$arrCol['width']  = 20;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'C';
		$arrCol['label'] = '2';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'SUB MENU';
		$arrCol['width']  = 30;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'PROSES';
		$arrCol['width']  = 40;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'DELETE';
		$arrCol['width']  = 50;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'EDIT';
		$arrCol['width']  = 50;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'ADD';
		$arrCol['width']  = 50;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'VIEW';
		$arrCol['width']  = 50;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$params 			   = array();
		$params['arrHead'] 	   = $mainCols;
		$params['orientation'] = 'P';
		$params['format'] 	   = 'A4';
		$this->load->library('Report', $params);
		
		$this->report->Open();
		$this->report->AddPage();
		
		$no = 1;
		foreach($rows as $row)
		{
			$arrData = array();
			$arrData[] = $no++;
			$arrData[] = strtoupper($row->npp);
			$arrData[] = $row->user_name;
			$arrData[] = strtoupper($row->kelompok);	
			$arrData[] = $row->nama_cabang;
			$arrData[] = $row->nama_cabang;
			$arrData[] = $row->nama_cabang;
			$this->report->InsertRow($arrData);
		}
			
		$this->report->ShowPDF('user_' . time());
	}
	
}