<?php

class Submenu extends MY_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('common_m');
		$this->load->model('user_m');
		$this->load->helper('pagination');
	}
	
	public function index($page=''){
		if (!checkACL(ACL_VIE)) show_error(ERROR_200);
		
		// PAGING
			$limit = 10;
			$offset = $page =='' ? 0 : ($page-1)*$limit;
					
		
		$res 	= $this->user_m->list_menu(0,0);
		$total 	= $res->num_rows();
		$rows	= $this->user_m->list_menu($limit,$offset);
			
		$data = array();
		$data['add']	= base_url().'admin/submenu/tambah';
		$data['hapus']	= 'admin/submenu/hapus';
		$data['print']	= 'admin/submenu/print';
		$data['page']	= paging('admin/submenu/index',$page,$total,$limit);
		$data['rows']	= $rows->result();
		$data['no']		= $offset;
		$data['menu_aktif']	= 'sitemap';
		$data['sub_aktif']	= 'submenu';
		$this->template->load('admin/sub_menu/index',$data);	
	}
	public function add(){
		if (!checkACL(ACL_ADD)) show_error(ERROR_200);
		$menu = $this->common_m->get_where('m_menu','*','parent = 0 order by ordering asc')->result();
		$data = array();
		$data['menu'] = $menu;
		$this->template->load('admin/sub_menu/tambah',$data,FALSE);			
	}
	public function tambahAct(){
		if (!checkACL(ACL_ADD)) show_error(ERROR_200);
		$result = true;
		$message = '';
		try
		{
			$sub_menu 	= trim(strtolower($this->getVar('sub_menu')));
			$url 		= $this->getVar('url');
			$id_menu	= $this->getVar('menu');
			
			
			// check sub menu jika sudah terdaftar tidak bisa ditambah lagi
			$check = $this->common_m->get_where('m_menu',' id_m_menu', " lower(nama_menu) = '{$sub_menu}' and parent = {$id_menu} ");
			if($check->num_rows() > 0)
				throw new exception('Sub Menu sudah terdaftar!');
			// GET ORDERING TERAKHIR 
			$row = $this->common_m->get_where('m_menu',' max(ordering)+1 as order', " parent = {$id_menu} ")->row();	
			$data = array(	'nama_menu'	=> $sub_menu,
							'icon' 		=> '',
							'title'		=> '',
							'link'		=> $url,
							'parent'	=> $id_menu,
							'ordering'	=> $row->order
						);
			$result = $this->common_m->inserted('m_menu',$data);
			if(!$result)
				throw new exception('Gagal tambah Menu!');
			$message = "Proses Berhasil!";
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
			echo json_encode(array("status"=>true,"msg"=>$message));	
	}
	public function edit($id_sub_menu=''){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$menu 	= $this->user_m->submenu_by_id($id_sub_menu)->row();
		$induk = $menu->induk; 
		$orders = $this->common_m->get_where('m_menu','*'," parent = {$induk} order by ordering asc")->result();
		$data = array();
		$data['row']		= $menu;
		$data['orders']		= $orders;
		$this->template->load('admin/sub_menu/edit',$data,FALSE);			
	}
	public function editAct(){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$result = true;
		$message = '';
		try
		{
			$id_sub_menu = $this->getVar('id_sub_menu');
			$sub_menu = $this->common_m->get_where('m_menu','*'," id_m_menu = {$id_sub_menu}")->row();
			$old_order = $sub_menu->ordering;
			
			$sub_menu_input = trim(strtolower($this->getVar('sub_menu')));
			$url			= trim(strtolower($this->getVar('url')));
			$input_order 	= $this->getVar('ordering');
			
			// get order berdasarkan input order(id_m_menu)
			$menu_order = $this->common_m->get_where('m_menu','*'," id_m_menu = {$input_order}")->row();
			
			// cek apakah nama menu diganti atau tidak
			// jika diganti check ketersedian menu
			if($sub_menu_input !== trim(strtolower($sub_menu->nama_menu)))
			{
				$check = $this->common_m->get_where('m_menu',' id_m_menu', " lower(nama_menu) = '{$sub_menu_input}' and parent = {$sub_menu->parent} ");
				if($check->num_rows() > 0)
					throw new exception(' Sub Menu sudah terdaftar!');
			}
				
			$data = array(	'nama_menu'		=> $sub_menu_input,
							'link'			=> $url,
							'ordering'		=> $menu_order->ordering	
							);
						
			$this->db->trans_begin();
			$result = $this->common_m->updated('m_menu',$data,array('id_m_menu'=>$id_sub_menu));
			if(!$result)
				throw new exception("Gagal update Sub menu");
			
			// cek apakah order diubah atau tidak
			// jika diubah order yang lama jadi order buat menu yang diganti(replace)
			
			if($old_order != $menu_order->ordering)
			{
				$result = $this->common_m->update_where('m_menu',array('ordering'=>$old_order),array('id_m_menu'=>$menu_order->id_m_menu));
				if(!$result)
					throw new exception("Gagal update order");
			}
			$this->db->trans_complete();
			$message = "Proses Update Berhasil!";	
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));	
	}
	public function hapus(){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$result = true;
		try {
			$arrID = $this->getVar('id_menu', TRUE);
			$listID = '';
			foreach ($arrID as $id) {
				
				$listID .= $id.',';
			}
			$listID .= '-1';
			$result = $this->common_m->sql_query("delete from m_menu where id_m_menu in({$listID})");
			if(!$result)
				throw new exception('Gagal hapus menu!');
			$message = 'Proses hapus berhasil!';
		} catch (Exception $e) {
			//Set error status from exception...
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));
	}
	
	public function cetak(){
		$rows 	= $this->user_m->list_menu(0,0);
		
		// 	REPORT		
		$mainCols 		  = array();

		$arrCol 		  = array();
		$arrCol['title']  = 'NO.';
		$arrCol['width']  = 10;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'R';
		$arrCol['label'] = '1';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'MENU';
		$arrCol['width']  = 50;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '2';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'SUB MENU';
		$arrCol['width']  = 70;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'URL';
		$arrCol['width']  = 80;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label']  = '4';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$params 			   = array();
		$params['arrHead'] 	   = $mainCols;
		$params['orientation'] = 'P';
		$params['format'] 	   = 'A4';
		$this->load->library('Report', $params);
		
		$this->report->Open();
		$this->report->AddPage();
		$no=1;
		$mn = '';
		foreach($rows->result() as $row)
		{
			if($mn != $row->menu){
				$mn = $row->menu;
				$disp_mn = $mn;	
			}
			else
				$disp_mn = '';
				
			$arrData = array();
			$arrData[] = $no++;
			$arrData[] = ucwords($disp_mn);
			$arrData[] = ucwords($row->sub_menu);
			$arrData[] = $row->link;
			$this->report->InsertRow($arrData);
		}
		$this->report->ShowPDF('menu_' . time());
	}
}