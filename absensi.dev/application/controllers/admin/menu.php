<?php

class Menu extends MY_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('common_m');
		$this->load->model('user_m');
		$this->load->helper('pagination');
	}
	
	public function index($page=''){
		if (!checkACL(ACL_VIE)) show_error(ERROR_200);
		
		// PAGING
			$limit = 10;
			$offset = $page =='' ? 0 : ($page-1)*$limit;
					
		
		$res 	= $this->user_m->get_menu(0,0);
		$total 	= $res->num_rows();
		$rows	= $this->user_m->get_menu($limit,$offset);
			
		$data = array();
		$data['add']	= base_url().'admin/menu/tambah';
		$data['hapus']	= 'admin/menu/hapus';
		$data['print']	= 'admin/menu/print';
		$data['page']	= paging('admin/menu/index',$page,$total,$limit);
		$data['rows']	= $rows->result();
		$data['no']		= $offset;
		$this->template->load('admin/menu/index',$data);	
	}
	public function add(){
		if (!checkACL(ACL_ADD)) show_error(ERROR_200);
	
		$data = array();
		$this->template->load('admin/menu/tambah',$data,FALSE);			
	}
	public function tambahAct(){
		if (!checkACL(ACL_ADD)) show_error(ERROR_200);
		$result = true;
		$message = '';
		try
		{
			$menu 	= trim(strtolower($this->getVar('nama_menu')));
			$url 	= trim(strtolower($this->getVar('url')));
			$icon 	= $this->getVar('icon');
			$title	= $this->getVar('title');
			$url 	= ($url) ?  $url : '#';
			
			// check menu jika sudah terdaftar tidak bisa ditambah lagi
			$check = $this->common_m->get_where('m_menu',' id_m_menu', " lower(nama_menu) = '{$menu}' and parent = 0 ");
			if($check->num_rows() > 0)
				throw new exception(' Menu sudah terdaftar!');
			// GET ORDERING TERAKHIR 
			$row = $this->common_m->get_where('m_menu',' max(ordering)+1 as order', " parent = 0 ")->row();	
			$data = array(	'nama_menu'	=> $menu,
							'icon' 		=> $icon,
							'title'		=> $title,
							'link'		=> $url,
							'parent'	=> 0,
							'ordering'	=> $row->order
						);
			$result = $this->common_m->inserted('m_menu',$data);
			if(!$result)
				throw new exception('Gagal tambah Menu!');
			$message = "Proses Berhasil!";
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
			echo json_encode(array("status"=>true,"msg"=>$message));	
	}
	public function edit($id_menu=''){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$menu = $this->common_m->get_where('m_menu','*'," id_m_menu = {$id_menu}")->row();
		$orders = $this->common_m->get_where('m_menu','*'," parent = 0 order by ordering asc")->result();
		$data = array();
		$data['row']		= $menu;
		$data['orders']		= $orders;
		$this->template->load('admin/menu/edit',$data,FALSE);			
	}
	public function editAct(){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$result = true;
		$message = '';
		try
		{
			$id_menu = $this->getVar('id_menu');
			$menu = $this->common_m->get_where('m_menu','*'," id_m_menu = {$id_menu}")->row();
			$old_order = $menu->ordering;
			
			$nama_menu 		= trim(strtolower($this->getVar('nama_menu')));
			$url 			= trim(strtolower($this->getVar('url')));
			$icon			= trim(strtolower($this->getVar('icon')));
			$title 			= trim(strtolower($this->getVar('title')));
			$input_order 	= $this->getVar('ordering');
			$url 			= ($url) ? $url : '#';
			// get order berdasarkan input order(id_m_menu)
			$menu_order = $this->common_m->get_where('m_menu','*'," id_m_menu = {$input_order}")->row();
			
			// cek apakah nama menu diganti atau tidak
			// jika diganti check ketersedian menu
			if($nama_menu !== trim(strtolower($menu->nama_menu)))
			{
				$check = $this->common_m->get_where('m_menu',' id_m_menu', " lower(nama_menu) = '{$nama_menu}' ");
				if($check->num_rows() > 0)
					throw new exception(' Menu sudah terdaftar!');
			}
				
			$data = array(	'nama_menu'		=> $nama_menu,
							'link'			=> $url,
							'icon'			=> $icon,
							'title'			=> $title,
							'ordering'		=> $menu_order->ordering	
							);
						
			$this->db->trans_begin();
			$result = $this->common_m->updated('m_menu',$data,array('id_m_menu'=>$id_menu));
			if(!$result)
				throw new exception("Gagal update menu");
			
			// cek apakah order diubah atau tidak
			// jika diubah order yang lama jadi order buat menu yang diganti(replace)
			if($menu->ordering != $menu_order)
			{
				$result = $this->common_m->update_where('m_menu',array('ordering'=>$old_order),array('id_m_menu'=>$input_order));
				if(!$result)
					throw new exception("Gagal update order");
			}
			$this->db->trans_complete();
			$message = "Proses Update Berhasil!";	
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));	
	}
	public function hapus(){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$result = true;
		try {
			$arrID = $this->getVar('id_menu', TRUE);
			$listID = '';
			foreach ($arrID as $id) {
				
				$listID .= $id.',';
			}
			$listID .= '-1';
			$result = $this->common_m->sql_query("delete from m_menu where id_m_menu in({$listID})");
			if(!$result)
				throw new exception('Gagal hapus menu!');
			$message = 'Proses hapus berhasil!';
		} catch (Exception $e) {
			//Set error status from exception...
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));
	}
	
	public function cetak(){
		$rows 	= $this->user_m->get_menu(0,0);
		
		// 	REPORT		
		$mainCols 		  = array();

		$arrCol 		  = array();
		$arrCol['title']  = 'NO.';
		$arrCol['width']  = 10;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'R';
		$arrCol['label'] = '1';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'MENU';
		$arrCol['width']  = 60;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '2';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'TITLE';
		$arrCol['width']  = 80;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$arrCol 		  = array();
		$arrCol['title']  = 'ICON';
		$arrCol['width']  = 50;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label']  = '4';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);
		
		$params 			   = array();
		$params['arrHead'] 	   = $mainCols;
		$params['orientation'] = 'P';
		$params['format'] 	   = 'A4';
		$this->load->library('Report', $params);
		
		$this->report->Open();
		$this->report->AddPage();
		$no=1;
		foreach($rows->result() as $row)
		{
			$arrData = array();
			$arrData[] = $no++;
			$arrData[] = ucwords($row->nama_menu);
			$arrData[] = ucwords($row->title);
			$arrData[] = $row->icon;
			$this->report->InsertRow($arrData);
		}
		$this->report->ShowPDF('menu_' . time());
	}	
}