<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will try guess the protocol, domain
| and path to your installation. However, you should always configure this
| explicitly and never rely on auto-guessing, especially in production
| environments.
|
*/
$config['operasional_db'] = array(
			'hostname' 		=> $hostname,
			'username' 		=> $username,
			'password' 		=> $password,
			'database' 		=> $database,
			'dbdriver' 		=> 'mssql',
			'dbprefix' 		=> '',
			'pconnect' 		=> FALSE,
			'db_debug' 		=> FALSE,
			'cache_on'		=> FALSE,
			'cachedir' 		=> '',
			'char_set' 		=> 'utf8',
			'dbcollat' 		=> 'utf8_general_ci',
			'swap_pre' 		=> '',
			'encrypt' 		=> FALSE,
			'compress' 		=> FALSE,
			'stricton' 		=> FALSE,
			'failover' 		=> array(),
			'save_queries' 	=> TRUE,
			'autoinit'		=> FALSE
		);

