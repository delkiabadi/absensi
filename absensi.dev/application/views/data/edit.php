<div class="row search">
	<form id="submit-form" action="<?=base_url()?>data/edit_act" method="post">
	<table class="form-input" >
		<thead>
			<tr>
				<td width="100px"> Nama Pegawai </td>
				<td><?php echo strtoupper($pegawai->nama_pegawai); ?>
					
				</td>
			</tr>
		</thead>
		<tbody id="form_edit">
			<tr>
				<td>Tanggal</td>
				<td><input type="text" name="tanggal" value="" readonly><td>
			</tr>
			<tr>
				<td>Jam Datang</td>
				<td><input type="text" name="tanggal" value="" readonly><td>
			</tr>
			<tr>
				<td>Jam Pulang</td>
				<td><input type="text" name="tanggal" value="" readonly><td>
			</tr>
			<tr>
				<td>Keterangan</td>
				<td><select name="keterangan" id="keterangan">
						<option value="">-- Pilih Keterangan --</option>
					</select>	
				<td>
			</tr>
			<tr>
				<td>Alasan</td>
				<td>
					<select name="alasan" id="alasan">
						<option value="">-- Pilih Alasan --</option>
					</select>
				<td>
			</tr>	
		</tbody>
		<tfoot>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" name="simpan" value="SIMPAN"><td>
			</tr>
		</tfoot>
	</table>	
	</form>
</div>
	<div>
		<table class="tabel cursor" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="10px">No.</th>
					<th>TANGGAL</th>
					<th>JAM DATANG</th>
					<th>JAM PULANG</th>
					<th>KETERANGAN</th>
					<th>ALASAN</th>
				</tr>
				
			</thead>
			<?php if($total > 0) { 
			
			
				foreach($rows as $row) {
			?>
				<tr class="tr_cursor" onclick="edit(<?=$row->id_m_pegawai?>,'<?=$row->tanggal?>')">
					<td class="tdCenter"><?php echo ++$no?></td>
					<td class="tdCenter"><?php echo date('d-m-Y',strtotime($row->tanggal));?></td>
					<td class="tdCenter"><?php echo is_null($row->jam_datang) ? '' : date('H:i:s',strtotime($row->jam_datang));?></td>
					<td class="tdCenter"><?php echo is_null($row->jam_pulang) ? '' : date('H:i:s',strtotime($row->jam_pulang));?></td>
					<td><?php echo !isset($this->KET_CONST[$row->keterangan]) ? '' : $this->KET_CONST[$row->keterangan];?></td>
					<td><?php echo !isset($this->ALASAN_CONST[$row->keterangan][$row->alasan]) ? '' : $this->ALASAN_CONST[$row->keterangan][$row->alasan];?></td>
				</tr>	
				<?php } } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="6" style="text-align:right;"><?php echo $page;?></td>
				</tr>
			<tfoot>
		</table>
	</div>
<script>
	function edit(id,tgl)
	{
			
		$.ajax({
			url : '<?=site_url()?>data/get_absen_by_id',
			type : 'post',
			
			data: {id_pegawai : id, tanggal : tgl},
			success : function(html){
				$("#form_edit").html(html);
			},error:function(x,y,z){
				alert(JSON.stringify(x));
			}
		})
	}
	function get_alasan(id)
	{
		$.ajax({
			url : '<?php echo site_url()?>data/get_alasan',
			type : 'post',
			data : {keterangan : id},
			success : function(html){
				$("#alasan").html(html);
				$("#surat_dokter").html('');
			},error:function(){
				alert('fail..');
			}	
		})
	}
	
	function get_surat_dokter(id){
		
		var ket = $("#keterangan").val();	
		var html ='';
		if(id==1 && ket==4)
			html = "<td>SURAT DOKTER</td><td><select name='surat_dokter'><option value='0'>Tidak Ada</option><option value='1'>Ada</option></td>";
		$("#surat_dokter").html(html);
	}	
	
	$("#submit-form").submit(function(e){
		var data = $(this).serialize();
		var urls = $(this).attr('action');
		$.ajax({
			url : urls,
			type : 'post',
			dataType : 'json',
			data : data,
			success: function(result)
			{
				if(result.status)
				{
					alert(result.msg);
				}else
					alert(result.msg);
			},error : function(x,y,z){
				alert(JSON.stringify(x.responseText));
			}
		})
	e.preventDefault();
})
</script>