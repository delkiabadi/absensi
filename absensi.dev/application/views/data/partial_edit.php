<input type="hidden" name="id_m_pegawai" value="<?=$row->id_m_pegawai?>">
<tr>
	<td>Tanggal</td>
	<td style="width:120px;"><input type="text" name="tanggal" value="<?=date('d-m-Y',strtotime($row->tanggal))?>" readonly></td>
</tr>
<tr>
	<td>Jam Datang</td>
	<td style="width:80px;"> <input type="text" name="jam_datang" value="<?php echo is_null($row->jam_datang) ? '' : date('H:i:s',strtotime($row->jam_datang));?>" readonly></td>
</tr>
<tr>
	<td>Jam Pulang</td>
	<td><input type="text" name="jam_pulang" value="<?php echo is_null($row->jam_pulang) ? '' : date('H:i:s',strtotime($row->jam_pulang));?>" readonly></td>
</tr>
<tr>
	<td>Keterangan</td>
	<td>
		<select name="keterangan" id="keterangan" onchange="get_alasan(this.value)">
		<option value="">-- Pilih Keterangan --</option>
		<?php 
			foreach($keterangan as $key=>$ket)
			{
		?>
			<option value="<?=$key?>"><?=$ket?></option>
		<?php	
			}
		?>
		</select>
	<td>
</tr>
<tr>
	<td>Alasan</td>
	<td>
		<select name="alasan" id="alasan" onchange="get_surat_dokter(this.value)">
			<option value="">-- Pilih Alasan --</option>
		</select>
	<td>
</tr>
<tr id="surat_dokter">
</tr>		
	