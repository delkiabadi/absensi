<thead>
				<tr>
					<th rowspan="2">No.</th>
					<th rowspan="2">EDIT</th>
					<th rowspan="2">NAMA</th>
					<th rowspan="2">TELAT</th>
					<th colspan="2">TIDAK ABSEN</th>
					<th rowspan="2">TIDAK MASUK</th>
					<th rowspan="2">HADIR</th>
				</tr>
				<tr>
					<th>DATANG</th>
					<th>PULANG</th>
				</tr>
			</thead>
			<tbody>
			<?php if($total > 0) {
				
				foreach($rows as $row) {
			?>
				<tr>
					<td><?php echo $no++;?></td>
					<td class="tdCenter"><span title="edit" class="edit" onclick="edit(<?=$row->id_m_pegawai?>);"> EDIT
				</span></td>
					<td><?php echo str_replace('\"','"',$row->nama);?></td>
					<td><?php echo $row->telat;?></td>
					<td><?php echo $row->tdk_absen_datang;?></td>
					<td><?php echo $row->tdk_absen_pulang;?></td>
					<td><?php echo $row->tdk_hadir;?></td>
					<td><?php echo $row->hari_kerja-$row->tdk_hadir;?></td>
				</tr>	
				<?php } } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan='7'>
						<?php echo $page;?>
					</td>	
				</tr>
			</tfoot>	
	<script>
		$(document).ready(function(){
			$('.pagination a').click(function()
			{		
				var url = $(this).attr('href');		
				var html = paging(url);
				$('.html_partial').html(html);
				return false;
			})
		})
	</script>	
			