<div class="row search">
    <div style="padding:10px 20px">
    <form method="post" id='main_form2' class='form_with_ajax'>   
    <table>
	<tr>
        <td>Periode</td>
		<td>
            <select name="bulan" id="bulan">
                <?php foreach($bulans as $key=>$nama_bulan){ $selected = ($key==$bulan) ? 'selected' : '';?>
				<option value="<?php echo $key;?>" <?php echo $selected;?>><?php echo $nama_bulan['long'];?></option>
                <?php } ?>
            </select>        
            <select name="tahun" id="tahun">
                <?php for($i=($tahun-3);$i<=$tahun;$i++) { $selected = ($i==$tahun) ? 'selected' :'';?>
				<option value="<?php echo $i;?>" <?php echo $selected;?>><?php echo $i;?></option>
                <?php } ?>
            </select>
		</td>
	</table>
    </form> 
</div>
</div>
<div class="row search">
</div>
<div id="konten">
	<table class="tabel html_partial" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th rowspan="2">No.</th>
					<th rowspan="2">EDIT</th>
					<th rowspan="2">NAMA</th>
					<th rowspan="2">TELAT</th>
					<th colspan="2">TIDAK ABSEN</th>
					<th rowspan="2">TIDAK MASUK</th>
					<th rowspan="2">HADIR</th>
				</tr>
				<tr>
					<th>DATANG</th>
					<th>PULANG</th>
				</tr>
			</thead>
			<tbody>
			<?php if($total > 0) {
				
				foreach($rows as $row) {
			?>
				<tr>
					<td><?php echo $no++;?></td>
					<td class="tdCenter"><span title="edit" class="edit" onclick="edit(<?=$row->id_m_pegawai?>);"> EDIT
				</span></td>
					<td><?php echo str_replace('\"','"',$row->nama);?></td>
					<td><?php echo $row->telat;?></td>
					<td><?php echo $row->tdk_absen_datang;?></td>
					<td><?php echo $row->tdk_absen_pulang;?></td>
					<td><?php echo $row->tdk_hadir;?></td>
					<td><?php echo $row->hari_kerja-$row->tdk_hadir;?></td>
				</tr>	
				<?php } } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan='7'>
						<?php echo $page;?>
					</td>	
				</tr>
			</tfoot>	
		</table>
</div>
<script>
	$(document).ready(function(){
			$('.pagination a').click(function(){
				alert("pager");
		var url = $(this).attr('href');
		var html = paging(url);
		$('.html_partial').html(html);
		return false;
	})
		
		$("#bulan,#tahun").change(function(){
			get_list_absen();
		})
	})
    function send(frm){
        if(confirm('Anda yakin untuk upload data ini?'))
        { 
            if($('#file_input').val() !='')
                frm.submit();
            else
            {
                alert('File belum dipilih!');
                return false;
            }    
        }
        else
            return false;
      } 
	function edit(id){
		var bulan = $("#bulan").val();
		var tahun = $("#tahun").val();
		window.location.href = '<?php echo site_url()?>data/update/'+id+'/'+bulan+'/'+tahun;
	}	
	function get_list_absen(){
		var bulan = $("#bulan").val();
		var tahun = $("#tahun").val();
		$.ajax({
			url 	: '<?=base_url()?>data/index',
			type 	: 'post',
			data 	: {bulan : bulan,tahun : tahun},
			success : function(html){
				$('.html_partial').html(html);
			},error : function(x,y,z){
				alert(JSON.stringify(x));
			}
		})
	}
	
</script>    
    