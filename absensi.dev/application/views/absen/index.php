<div class="row search">
    <div style="padding:10px 20px">
    <form method='post' action="<?php site_url()?>absen/proses" enctype="multipart/form-data" id="submit_form">    
    <table>
	<tr>
            <td>Periode</td>
		<td>
                    <select name="bulan" id="bulan">
                        <?php foreach($bulan as $key=>$nama_bulan){ $selected = ($key==$cur_bulan) ? 'selected' : '';?>
			<option value="<?php echo $key;?>" <?php echo $selected;?>><?php echo $nama_bulan['long'];?></option>
                        <?php } ?>
                    </select>
                    
                    <select name="tahun" id="tahun" readonly>
                        <?php for($i=($cur_tahun-3);$i<=$cur_tahun;$i++) { $selected = ($i==$cur_tahun) ? 'selected' :'';?>
			<option value="<?php echo $i;?>" <?php echo $selected;?>><?php echo $i;?></option>
                        <?php } ?>
                    </select>
		</td>
               
	</tr>
        <tr>
            <td>FILE (xls) </td>
            <td> <input type="file" name="file_upload"  id="file_input"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="UPLOAD" id="upload" onclick="return send(this);" ></td>
        </tr>
        
	</table>
    </form> 
</div>
</div>
<div id="konten">
	<table class="tabel html_partial" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th rowspan="2">No.</th>
					<th rowspan="2">NAMA</th>
					<th rowspan="2">TELAT</th>
					<th colspan="2">TIDAK ABSEN</th>
					<th rowspan="2">TIDAK MASUK</th>
					<th rowspan="2">HADIR</th>
				</tr>
				<tr>
					<th>DATANG</th>
					<th>PULANG</th>
				</tr>
			</thead>
			<?php if($total > 0) {
				$no = 1;
				foreach($rows as $row) {
			?>
				<tr>
					<td><?php echo $no++;?></td>
					<td><?php echo str_replace('\"','"',$row->nama);?></td>
					<td><?php echo $row->telat;?></td>
					<td><?php echo $row->tdk_absen_datang;?></td>
					<td><?php echo $row->tdk_absen_pulang;?></td>
					<td><?php echo $row->tdk_hadir;?></td>
					<td><?php echo $row->hari_kerja-$row->tdk_hadir;?></td>
				</tr>	
				<?php } } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan='7'>
						<?php echo $page;?>
					</td>	
				</tr>
			</tfoot>	
		</table>
</div>
<script>
	$(document).ready(function(){
		$('.pagination a').click(function(){
		var url = $(this).attr('href');
		var html = paging(url);
		$('.html_partial').html(html);
		return false;
	})
		
	})
      function send(frm){
            if(confirm('Anda yakin untuk upload data ini?'))
            { 
                if($('#file_input').val() !='')
                    frm.submit();
                else
                {
                    alert('File belum dipilih!');
                    return false;
                }    
            }
            else
               return false;
      }  
</script>    
    