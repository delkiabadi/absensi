<thead>
				<tr>
					<th rowspan="2">No.</th>
					<th rowspan="2">NAMA</th>
					<th rowspan="2">TELAT</th>
					<th colspan="2">TIDAK ABSEN</th>
					<th rowspan="2">TIDAK MASUK</th>
					<th rowspan="2">HADIR</th>
				</tr>
				<tr>
					<th>DATANG</th>
					<th>PULANG</th>
				</tr>
			</thead>
			<?php if($total > 0) {
				$no = 1;
				foreach($rows as $row) {
			?>
				<tr>
					<td><?php echo $no++;?></td>
					<td><?php echo str_replace('\"','"',$row->nama);?></td>
					<td><?php echo $row->telat;?></td>
					<td><?php echo $row->tdk_absen_datang;?></td>
					<td><?php echo $row->tdk_absen_pulang;?></td>
					<td><?php echo $row->tdk_hadir;?></td>
					<td><?php echo $row->hari_kerja-$row->tdk_hadir;?></td>
				</tr>	
				<?php } } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan='7'>
						<?php echo $page;?>
					</td>	
				</tr>
			</tfoot>