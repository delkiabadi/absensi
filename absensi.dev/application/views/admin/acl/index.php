<div class="row top" style="min-height:40px;">
	<ul class='act'>
		<? if (checkACL(ACL_ADD)) { ?>
		<li id="save" onclick="simpan();">
			<span class="save" title="SAVE"></span>
		</li>
		<?php } ?>	
		<li id="print">
			<span class="print" title="Print"> </span>
		</li>
	</ul>
</div>

<!-- TITLE -->
<div class="row header_title">
   ACCESS CONTROL
</div>
<div class="row search">
	<table>
		<tr>
			<td>Cari</td>
			<td>:</td>
			<td>
				<select name="kelompok"  width="100px" id="kelompok">
					<option value='0'>-- Pilih Kelompok --</option>
					<?php foreach($kelompok as $dt) {?>
					<option value="<?php echo $dt->id_m_kelompok;?>"><?php echo $dt->kelompok;?></option>
					<?php } ?>
				</select>
			</td>
		</tr>	
	</table>
</div>
<div id="konten">
	<form method="post" id='main_form' class='form_with_ajax'>
	<input type='hidden' id="ids" name="ids">
	<div class='bg_table'>				
		<table class="tabel html_partial" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class='tdNo' rowspan="2">NO</th>
					<th width="30%" rowspan="2">MENU</th>
				
					<th colspan="<?php echo count($arrConstACL); ?>" class="tdCenter">PRIVILEGES</th>
				</tr>
				<tr>
					<?php foreach($arrConstACL as $k=>$v) { ?>
					<th style="width:60px;" class="tdCenter"><?php echo $arrDescACL[$k]; ?></th>
					<?php } ?>
			</tr>
			</thead>
			<tbody id='tblBody'>	
			<?php 
				$no =1;
				$no_ = 1;
				foreach($menus as $m) 
				{	
			?>
					<tr>
						<td><?php 	if($m['tree'] == 'parent')
										echo $no.'.';		
							?>
						</td>				
						<td style='text-align:left;'>
							<?php 	if($m['tree'] == 'child')
										echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp".ucwords($m['menu']);
									else{
										echo ucwords($m['menu']);
										$no++;
										
									}
							
							?>
						</td>
						
						<?php foreach($arrConstACL as $k=>$v) { ?>
							
							<td align="center">
							<?php if($m['url'] != '#') { ?>
								<input type="checkbox" name="chk[<?php echo $no_; ?>][]" value="<?php echo $v; ?>" >
							<?php } ?>
							</td>
							
						<?php  } ?>
					</tr>
				<?php $no_++;} ?>
				
				
			</tbody>
			<tfoot>
				<tr>
					<td colspan="8"> </td>
				</tr>
			</tfoot>
			
			
		</table>
		
	</div>	
	</form>
</div>
<script>
$("#kelompok").change(function(){
	$("#ids").val($(this).val());
	$.ajax({
		url 	: '<?php echo base_url();?>admin/acl/index',
		type 	: 'post',
		data 	: {id_kelompok : $(this).val()},
		success : function(html){
			$("#tblBody").html(html);
		},error : function(xhr, status, error){
			//var err = eval("(" + xhr.responseText + ")");
			//alert(xhr.responseText);
			alert('something error..');
		}
	})
})
function simpan()
{
	id_kelompok = $("#kelompok").val();
	if(id_kelompok == '0')
		alert('Kelompok belum dipilih');
	else	
	{
		var data = $("form#main_form").serialize();
		$.ajax({
			url 		: '<?php echo base_url()?>admin/acl/editAct',
			dataType	: 'json',
			data 		: data,
			type		: 'post',
			success		: function(response){
				alert(response.msg)
			},error : function(){
				alert('Something error..');
			}		
		})	
	}
	return false;
}
function print(){
	$id = $("#kelompok").val();
	if($id=='0')
		alert('Kelompok belum dipilih');
	else{
		var url = get_url();
		openPop(url+'cetak/'+$id);
	}	
}
</script>
