<div class="judul tdCenter">
      FORM EDIT KELOMPOK
</div>
<!-- content -->
<div id="konten">
	<form method="post" id='main_form' class='form_with_ajax' action='<?=base_url()?>admin/group/editAct'>
	
		<table class="tabel html_partial" class="display" cellspacing="0" width="100%">	
			<tr>
				<td class='tdRight'>Kelompok</td>
				<td>
					<input type='text' name="nama_kelompok" id='nama_kelompok' value="<?php echo $row->kelompok;?>" class='form-control' required>
					<input type='hidden' name="id_kelompok" value="<?php echo $row->id_m_kelompok;?>">
				</td>
			</tr>
			
			<tr>
				<td class='tdRight'>Keterangan</td>
				<td>
						<textarea name='keterangan'  required class="form-control">
							<?php echo trim($row->keterangan);?>
						</textarea>
				</td>
			</tr>
			
			 <tr>
				<td>&nbsp;</td>
				<td colspan='3'>
					<input type='submit' value='SIMPAN' class="btn btn-primary" name='submit'>
					<input type='reset' value='RESET' class="btn btn-danger" id='reset'>
				</td>
			 </tr>
		</table>	
	</form>
</div>

<script>
$("form#main_form").submit(function(e){
	var link = $(this).attr('action');
	var data = $(this).serialize();

	$.ajax({
		url : link,
		type : 'post',
		data : data,
		dataType : 'json',
		success : function(response){
			if(response.status)
			{
				alert(response.msg);	
			}else
				alert(response.msg);
		},error: function(){
			alert('SESSION ANDA HABIS');
			window.close();
		}
	})
	e.preventDefault();
})
</script>