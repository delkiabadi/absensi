<div class="judul tdCenter">
      FORM TAMBAH USER 
</div>
<!-- content -->
<div id="konten">
	<form method="post" id='main_form' class='form_with_ajax' action='<?=base_url()?>admin/user/tambahAct'>
		<table class="tabel html_partial" class="display" cellspacing="0" width="100%">	
			<tr>
				<td class='tdRight'>Kelompok</td>
				<td>
					<select name='id_m_kelompok' id='id_m_kelompok' class='form-control' required>
						<option value=''>-- Pilih Kelompok --</option>
						<?php foreach($kelompok as $kel) { ?>
							<option value='<?php echo $kel->id_m_kelompok?>'><?php echo $kel->kelompok;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td class='tdRight'>Username</td>
				<td><input type='text' name='username' id='username' class='form-control' required></td>
			</tr>
			<tr>
				<td class='tdRight'>Password</td>
				<td><input type='password' name='password' id='password' class='form-control' required></td>
			</tr>
			<tr>
				<td class='tdRight'>Konfirmasi Password</td>
				<td><input type='password' name='konfirmasi_password' id='konfirmasi_password' class='form-control' required></td>
			</tr>
			<tr>
				<td class='tdRight'>NPP</td>
				<td><input type='text' name='npp' id='npp' class='form-control' required></td>
			</tr>
			<tr>
				<td class='tdRight'>Status</td>
				<td>
					<select name='status' id='status' class='form-control' required>
						<option value=''>-- Pilih Status --</option>
						<option value='0'>Aktif</option>
						<option value='1'>Non Aktif</option>
					</select>
				</td>
			</tr>
			 <tr>
				<td>&nbsp;</td>
				<td colspan='3'>
					<input type='submit' value='SIMPAN' class="btn btn-primary" name='submit'>
					<input type='reset' value='RESET' class="btn btn-danger" id='reset'>
				</td>
			 </tr>
		</table>
	</form>
</div>

<script>
$("form#main_form").submit(function(e){
	var link = $(this).attr('action');
	var data = $(this).serialize();
	
	$.ajax({
		url : link,
		type : 'post',
		data : data,
		dataType : 'json',
		success : function(response){
			if(response.status)
			{
				alert(response.msg);
				opener.location.reload()
				window.close()				
			}else{ 
				alert(response.msg);
				
			}
		},error: function(){
			alert('SESSION ANDA HABIS');
			window.close();
		}
	})
	e.preventDefault();
})
</script>