<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
	class MY_Controller extends CI_Controller {
	var $header_content = '';
	var $user;
	var $active;
	var $KET_CONST;
	var $ALASAN_CONST;
    public function __construct(){
		parent::__construct();
		
		$this->load->model('common_m');
		$active = $this->uri->segment(1).'/'.$this->uri->segment(2);
		$arrConstACL = $this->config->item('ACL_CONST');
		$this->KET_CONST	= $this->config->item('KET_CONST');
		$this->ALASAN_CONST	= $this->config->item('ALASAN_CONST');
		foreach ($arrConstACL as $k=>$v) define(strtoupper($k),  $v);
		
		
		if ($this->checkLogin() != TRUE) {
			$this->loginPage();
		}
	}
 
	protected function checkLogin() {
			$flagLogin = $this->session->userdata('flag_login');
			return $flagLogin;
	}
	private function loginPage() {
			$this->load->library('form_validation');

			$rules = array();
			$rules[] = array('field' => 'user_name', 'label' => 'User ID', 'rules' => 'trim|required');
			$rules[] = array('field' => 'pass_word', 'label' => 'Password', 'rules' => 'trim|required');

			$this->form_validation->set_rules($rules); 
				
			$this->load->view('login');
			log_message('debug', "Sent login page");
	}
	// get variabel
	protected function getVar($name, $throwOnError = FALSE) {
			$var = $this->input->post($name);
			if ($throwOnError) {
				if ($var === FALSE)
					throw new Exception("Variabel {$name} harus dilengkapi.");
				else
					return $var;
			} else
				return (($var === FALSE) ? 'null' : $var);
	}
	// function override
	public function index(){
		show_error('HALAMAN TIDAK DITEMUKAN',202);
	}
	
	public function edit(){
		show_error('HALAMAN TIDAK DITEMUKAN',202);
	}
	public function tambah(){
		show_error('HALAMAN TIDAK DITEMUKAN',202);
	}
	public function hapus(){
		show_error('HALAMAN TIDAK DITEMUKAN',202);
	}
	public function aprove(){
		show_error('HALAMAN TIDAK DITEMUKAN',202);
	}
	public function reject(){
		show_error('HALAMAN TIDAK DITEMUKAN',202);
	}
	public function cetak(){
		show_error('HALAMAN TIDAK DITEMUKAN',202);
	}
	public function expiredPage(){
		$this->load->view('session_exp');
	} 
 }